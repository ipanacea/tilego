var baseComponent = require ('tilego/components/base_component');
var responsiveStyle = require ('tilego/windows/responsive_style');
var Util = require('tilego/util/Util');
var logger = logging.getLogger("base_window.js");
var orientationEventSubscribers;
var OpenedWindows = new Array();


(function(){
	Ti.API.info("initializing orientation event manager");
	orientationEventSubscribers = {};
	function orient(event){
		responsiveStyle.resetStyles();
		var lastOpenedWindow = Util._.last(OpenedWindows);
		if(orientationEventSubscribers.hasOwnProperty(lastOpenedWindow)){
			var lastOpenedWindowThisPointer = orientationEventSubscribers[lastOpenedWindow];
			lastOpenedWindowThisPointer.orient({event:event});
		}
	};
	Ti.Gesture.addEventListener('orientationchange',orient);
	
})();

/**
* This is base window class which wraps titanium Window as commonJS module
*
* @class BaseWindow
* @constructor
* @extends BaseComponent
* @module tilego
* @submodule windows
*/
var androidBackButtonEventHandler = null;
function BaseWindow(_args) {
	if(_args){
		if(!_args.hasOwnProperty("name")){
			_args["name"] = "baseWindow";
		}	
	};
	baseComponent.call(this,_args);
	/** @property containerWindow titanium window which holds all the views
	  */
	this.containerWindow = null;
	this.mainView = null;
	this.activityIndicator = null;
	this._isWindowOpen = false;
	
}

BaseWindow.prototype = Object.create(baseComponent.prototype);
BaseWindow.prototype.constructor = BaseWindow;
/**
 * initates the container /window i.e. calls
 * {{#crossLink "createContainerWindow:method"}}{{/crossLink}} and
 * {{#crossLink "containerView:method"}}{{/crossLink}}
 * @method initContainer
 */
BaseWindow.prototype.initContainer = function() {
	this.createContainerWindow();
	this.containerView();
};

/**
 * method to initialize the window, calling this will create an empty window 
 * and sets fullscreen and navBarHidden to true
 * @method createContainerWindow
 */
BaseWindow.prototype.createContainerWindow = function() {
	this.containerWindow = Ti.UI.createWindow({
		 	navBarHidden: true,
		 	fullscreen:true,
	});
};

/**
 * method to initialize the window, calling this will create an empty containerView 
 * which occupies the width and height of the screen
 * @method createContainerView
 */
BaseWindow.prototype.createContainerView = function() {
	var that = this;
	this.containerView = Ti.UI.createView({
		height:"auto",
		width:"auto",
	});
	this.containerWindow.add(that.containerView);
};

/**
 * method to destroy the component, it recursives loops throughs all the views and destroys them.
 *  (removes all child views and sets them to null and eventually sets the parent to null)
 * Note: calling this will close the window and will set it to null
 * @method destroy
 */
BaseWindow.prototype.destroy = function() {
	this.call_base(this,"destroy");
	this.destroyView(this.containerWindow);
	this.closeWindow();
	this.containerWindow = null;
};

/**
 * returns the container view of the component
 *
 * @method getContainerWindow
 * @return [{Ti.UI.Window}] [Ti.UI.Window](http://docs.appcelerator.com/titanium/3.0/#!/api/Titanium.UI.Window) 
 */
BaseWindow.prototype.getContainerWindow = function() {
	return this.containerWindow;
};
/**
 * Generates a header for a window, 
 * @param {Object} optional argument to set the title of the header title:name
 * @optional
 * @method generateHeader
 * @return {Ti.UI.View} [Ti.UI.View](http://docs.appcelerator.com/titanium/3.0/#!/api/Titanium.UI.View) 
 */
BaseWindow.prototype.generateHeader = function(_args) {
	var headerheight = "45dp";
	var headerView = Ti.UI.createView({
		height : headerheight,
		width : "auto",
		top : "0dp",
		backgroundColor : "transparent"

	});
	var headerViewBackground = Ti.UI.createView({
		height : headerheight,
		width : "auto",
		top : "0dp",
		backgroundColor : "#4eb660",
		opacity : 1,

	});
	var headerTitle = Ti.UI.createLabel({
		text : _args.title,
		font : {
			fontSize : 18,
			fontFamily : "OpenSans-Light.ttf"
		},
		height : "25dp",
		color : 'white',
		top : "10dp",
	});
	headerView.add(headerViewBackground);
	headerView.add(headerTitle);

	var backButton = Ti.UI.createButton({
		left : "5dp",
		top : "0dp",
		enabled : 'true',
		width : "50dp",
		height : "50dp",
		backgroundImage : '/images/backArrow.png'
	});
	var that = this;
	backButton.addEventListener('click', function(e) {
		that.closeWindow();
	});

	headerView.add(backButton);
	return headerView;
};


/**
 * method to handle orientation changes. This also triggers a style rerender of the current window
 * @param {Event} is passed to the function which has info on current orientation.
 * @method orient
 */
BaseWindow.prototype.orient = function(_args){
	this._renderStyles(true);
};


/**
 * opens the titanium window
 * in android, it automatically attaches an event listener for the android backbutton to the current window
 * @param {Object} optional arguments can be sent for animation purposes
 * @optional
 * @method openWindow
 */
BaseWindow.prototype.openWindow = function(_args) {
	try {
		Ti.API.info("Opening Window: " + this.getName());
		if(!this.containerWindow["name"])
			this.containerWindow["name"] = this.name;
		if (this.Util.Device.properties.osname === 'android') {
			if (this.containerWindow) {
				var that = this;
				androidBackButtonEventListener = function(e) {
					that.closeWindow();

				};
				this.containerWindow.addEventListener('android:back', androidBackButtonEventListener);
			}
		}
		// apply tss styles on the window and child views
		var start = new Date().getTime();
		this._renderStyles();
		var elapsed = new Date().getTime() - start;
		logger.info("time taken for applying style on window: " + this.name + " is : "+ elapsed);
		if (_args) {
			if (_args.animate) {
				Ti.API.info("Window open args: anitmate: " + _args.animate, " transitionStyle: " + _args.transitionStyle);
				this.containerWindow.open(_args.transitionStyle);
			}
		} else
			this.containerWindow.open({animated:true});
		this._isWindowOpen = true;
		OpenedWindows.push(this.name);
		orientationEventSubscribers[this.Util._.last(OpenedWindows)] = this;
	
	} catch(error) {
		Ti.API.error("Tried to open an uninitialized window :" + this.getName() + " error: " + error.message);
	}
};

/**
 * closes the titanium window
 * in android, it automatically removes the event listener for the android backbutton 
 * @param {Object} optional arguments can be sent for animation purposes
 * @optional
 * @method closeWindow
 */
BaseWindow.prototype.closeWindow = function(_args) {
	try {
		Ti.API.info("Closing Window: " + this.getName());
		if (this.Util.Device.properties.osname === 'android') {
			if (this.containerWindow) {
				this.containerWindow.removeEventListener('android:back', androidBackButtonEventListener);
			}
		}

		if (_args) {
			if (_args.animate) {
				Ti.API.info("Window close args: animate: " + _args.animate, " transition: " + _args.transitionStyle);
				this.containerWindow.close(_args.transitionStyle);
			}
		} else
			this.containerWindow.close({animated:true});
		this._isWindowOpen = false;
	} catch(error) {
		Ti.API.error("Tried to close an uninitialized window: " + this.getName() + " error: " + error.message);
	}

};

BaseWindow.prototype.add = function(view){
	if(instanceOfTitaniumView(view)){
		if(this._isWindowOpen){
			this.applyStyle(view);
		}
		this.getContainerView().add(view);
	}
};

BaseWindow.prototype._renderStyles = function(force){
	if(force || !this.styleApplied){
		var that = this;
		this.applyStyle(that.getContainerWindow(), true);
		this.styleApplied = true;
		
	}
};

BaseWindow.prototype.getActivityIndicator = function(_args) {
	var indicatorView = Ti.UI.createView({
		height : "50dp",
		width : "180dp",
		backgroundColor : '#4eb660',
		//borderRadius: "2dp",
		//borderColor:"black",
		opacity : 0.7
	});

	var style;
	if (this.Util.Device.properties.osname !== "android") {
		style = Ti.UI.iPhone.ActivityIndicatorStyle.DARK;
	} else {
		style = Ti.UI.ActivityIndicatorStyle.BIG_DARK;
	}

	var activityIndicator = Ti.UI.createActivityIndicator({
		color : 'black',
		font : {
			fontFamily : 'OpenSans-Semibold',
			fontSize : 16,
		},
		message : 'Loading...',
		style : style,
	});
	activityIndicator.show();
	indicatorView.add(activityIndicator);

	return indicatorView;
};
BaseWindow.prototype.createActivityIndicatorRow = function(_args) {
	var activityRow = Ti.UI.createTableViewRow({
		height : "300dp",
		width : "100%",
		selectedColor : "none",
		backgroundSelectedColor : "none",
		top : "10dp",
	});

	activityRow.add(this.getActivityIndicator());
	return activityRow;
};
BaseWindow.prototype.generateError = function(_args) {
	var that = _args.this;
	var errorMainView = Ti.UI.createView({
		backgroundColor : '#8A9692',
		opacity : 0.8,
	});
	var errorView = Ti.UI.createView({
		layout : 'vertical',
		height : "200dp",
		width : "250dp",
		backgroundColor : "#2eb0c4",
		borderRadius : "10dp",
	});

	var errorImage = Ti.UI.createImageView({
		image : '/images/Sad_Cloud.png',
		height : "100dp",
		width : "100dp",

	});

	var errorLabel = Ti.UI.createLabel({
		text : _args.statusText,
		color : 'white'
	});

	var closeButton = Ti.UI.createButton({
		title : 'Close',
		width : "80dp",
		backgroundColor : 'red',
		borderRadius : "10dp",
		color : 'white',
		opacity : 0.7

	});

	closeButton.addEventListener('click', function(e) {
		that.containerView.remove(errorMainView);
	});

	//errorView.add(errorImage);
	errorView.add(errorLabel);
	//errorView.add(closeButton);
	errorMainView.add(errorView);

	that.containerView.add(errorMainView);

};



module.exports = BaseWindow; 