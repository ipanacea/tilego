
var baseLayout = require ('tilego/windows/layouts/responsive_base_window');
var style = {
	default:{
		containerWindow:{
			navBarHidden: true,
			fullscreen:true,
			backgroundColor:"#3DBFD9",
		},
		containerView:{
			backgroundColor:"#4ba5be",
			height:"auto",
			width:"auto",
		}
	},
	tablet:{
		portrait:{},
		landscape:{},
		containerWindow:{
			navBarHidden: true,
			fullscreen:true,
			backgroundColor:"#3DBAD9",
		},
		containerView:{
			backgroundColor:"#4ba5be",
			height: Ti.Gesture.isPortrait()?"40%":"40%",
			width: Ti.Gesture.isPortrait()?"60%":"70%",
			borderRadius:"5dp",
		}
	},
	phone:{
		containerWindow:{
			navBarHidden: true,
			fullscreen:true,
			backgroundColor:"#4ba5be",
			
		},
		containerView:{
			backgroundColor:"#4ba5be",
			height:"90%",
			width:"80%",
			borderRadius:"5dp",
		}
	},
};

ModalViewLayout = function(_args){
	this.setPhoneOrientationModes({
		modes:[Ti.UI.PORTRAIT,Ti.UI.UPSIDE_PORTRAIT],
	});
	this.setTabletOrientationModes({
		modes:[Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT, Ti.UI.LANDSCAPE_LEFT, Ti.UI.LANDSCAPE_RIGHT,],
	});
	baseLayout.call(this,_args);
	
};

ModalViewLayout.prototype = Object.create(baseLayout.prototype);
ModalViewLayout.prototype.constructor = ModalViewLayout;

ModalViewLayout.prototype.orient = function(_args){
	if(this.Util.Device.isTablet()){
		if(_args.event.portrait === true) {
			this.containerView.setHeight("70%");
			this.containerView.setWidth("60%");
		}else{
			this.containerView.setHeight("60%");
			this.containerView.setWidth("70%");
		}
				
	}
	this.call_base(this,"orient",_args);
};



ModalViewLayout.prototype.createTabletContainerWindow = function(_args){
	var newStyle = this.Util.functions.mergeObjs(style.tablet.containerWindow, this.extractStyle("containerWindow", _args.style));
	this.containerWindow = Ti.UI.createWindow(newStyle);
	this.containerWindow["name"] = this.name;
};

ModalViewLayout.prototype.createTabletContainerView = function(_args){
	var newStyle = this.Util.functions.mergeObjs(style.tablet.containerView, this.extractStyle("containerView", _args.style));
	this.containerView = Ti.UI.createView(newStyle);
	this.containerView["name"] = "containerView:" +this.name; 
	var that = this;
	this.containerWindow.add(that.containerView);
};

ModalViewLayout.prototype.createPhoneContainerWindow = function(_args){
	var newStyle = this.Util.functions.mergeObjs(style.phone.containerWindow, this.extractStyle("containerWindow", _args.style));
	this.containerWindow = Ti.UI.createWindow(newStyle);
	
};

ModalViewLayout.prototype.createPhoneContainerView = function(_args){
	var newStyle = this.Util.functions.mergeObjs(style.phone.containerView, this.extractStyle("containerView", _args.style));
	this.containerView = Ti.UI.createView(newStyle);
	var that = this;
	this.containerWindow.add(that.containerView);
	
};

ModalViewLayout.prototype.setData = function(_args){
};

module.exports = ModalViewLayout;
