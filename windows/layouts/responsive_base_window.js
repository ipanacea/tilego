
var baseView = require ('tilego/windows/base_window');
var Util = require('tilego/util/Util');

/**
* This is base window class which handles responsive layoyuts and it wraps titanium Window as commonJS module
*
* @class ResponsiveBaseWindow
* @constructor
* @extends BaseWindow
* @module tilego
* @submodule layouts
*/

ResponsiveBaseWindow = function(_args){
	baseView.call(this,_args);
	if(this.Util.Device.isTablet()){
		this.initTablet(_args);
	}
	else{
		this.initPhone(_args);
	}
	this.setOrientationModes(_args);
};

ResponsiveBaseWindow.prototype = Object.create(baseView.prototype);
ResponsiveBaseWindow.prototype.constructor = ResponsiveBaseWindow;

/**
 * abstract method to init the layout for the tablet
 * @method initTablet
 */
ResponsiveBaseWindow.prototype.initTablet = function(_args){
	this.createTabletContainerWindow(_args);
	this.createTabletContainerView(_args);
};

/**
 * abstract method to define the containerWindow for tablets
 * @method createTabletContainerWindow
 */
ResponsiveBaseWindow.prototype.createTabletContainerWindow = function(_args){
	
};

/**
 * abstract method to define the containerView for tablets
 * @method createTabletContainerView
 */
ResponsiveBaseWindow.prototype.createTabletContainerView = function(_args){
	
};

/**
 * abstract method to init the layout for the phone
 * @method initPhone
 */
ResponsiveBaseWindow.prototype.initPhone = function(_args){
	this.createPhoneContainerWindow(_args);
	this.createPhoneContainerView(_args);
};

/**
 * abstract method to define the containerWindow for phones
 * @method createPhoneContainerWindow
 */
ResponsiveBaseWindow.prototype.createPhoneContainerWindow = function(_args){
	
};

/**
 * abstract method to define the containerView for phones
 * @method createPhoneContainerView
 */
ResponsiveBaseWindow.prototype.createPhoneContainerView = function(_args){
	
};


/**
 * method to set orientation modes for the Ti.UI.Window.
 * This is the actual function which addes the modes to the window
 * @method setOrientationModes
 */
ResponsiveBaseWindow.prototype.setOrientationModes = function(_args){
	if(this.containerWindow){
		if(this.Util.Device.isTablet()){
			this.containerWindow.orientationModes = this.TabletOrientationModes;		
		}	
		else{
			this.containerWindow.orientationModes = this.PhoneOrientationModes;
		}		
	}else{
		Ti.API.error("Container window not initialzed for " + this.getName());
	}
};

/**
 * method to set orientation modes for the Ti.UI.Window in phone mode
 * @method setPhoneOrientationModes
 * @param {JSON} containing modes array
 */
ResponsiveBaseWindow.prototype.setPhoneOrientationModes = function(_args){
	if(_args){
		if(_args.modes && _args.modes.constructor === Array){
			this.PhoneOrientationModes = _args.modes; 			
		}
	}
};

/**
 * method to set orientation modes for the Ti.UI.Window in tablet mode
 * @method setPhoneOrientationModes
 * @param {JSON} containing modes array
 */
ResponsiveBaseWindow.prototype.setTabletOrientationModes = function(_args){
	if(_args){
		if(_args.modes && _args.modes.constructor === Array){
			this.TabletOrientationModes = _args.modes; 			
		}
	}
};

ResponsiveBaseWindow.prototype.closeWindow = function(_args){
	if(Util._.last(OpenedWindows) === this.getName()){
		delete orientationEventSubscribers[OpenedWindows.pop()];
	}
	this.call_base(this,"closeWindow",_args);
};


ResponsiveBaseWindow.prototype.extractStyle = function(elementName, styleJson){
	if(isStyleValid(styleJson)){
		if(this.Util.Device.isTablet()){
			if(styleJson.hasOwnProperty("tablet")){
				if(styleJson.tablet.hasOwnProperty(elementName)){
					return styleJson.tablet[elementName];
				}
			}
			if(styleJson.hasOwnProperty("default")){
				if(styleJson.tablet.hasOwnProperty(elementName)){
					return styleJson.tablet[elementName];
				}
			}
		}
		else{
			if(styleJson.hasOwnProperty("phone")){
				if(styleJson.tablet.hasOwnProperty(elementName)){
					return styleJson.phone[elementName];
				}
			}
			if(styleJson.hasOwnProperty("default")){
				if(styleJson.tablet.hasOwnProperty(elementName)){
					return styleJson.phone[elementName];
				}
			}
		}
	}
};

//private function
function isStyleValid(styleJson){
	if(styleJson){
		if(styleJson.hasOwnProperty("default") && (styleJson.hasOwnProperty("tablet") || styleJson.hasOwnProperty("phone"))){
			return true;
		}			
	}
	return false;
}

module.exports = ResponsiveBaseWindow;
