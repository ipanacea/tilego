var logger = logging.getLogger("resposiveStyle.js");

/**
* This class all the tss styling information
*
* @class ResponsiveStyle
* @constructor
* @module tilego
* @submodule layouts
*/
var OS_ANDROID = $tl.device.isAndroid();
var OS_IOS = $tl.device.isAndroid();

var ResponsiveStyle = (function () {
  var general = {};
  var phone = {};
  var tablet = {};
  var tablet_portrait = {};
  var tablet_landscape = {};
 
  var IDENTITY_TRANSFORM = OS_ANDROID ? Ti.UI.create2DMatrix() : undefined;
  var RESET = {
  	bottom: null,
	left: null,
	right: null,
	top: null,
	height: null,
	width: null,
	shadowColor: null,
	shadowOffset: null,
	backgroundImage: null,
	backgroundRepeat: null,
	center: null,
	layout: null,
	backgroundSelectedColor: null,
	backgroundSelectedImage: null,

	// non-null resets
	opacity: 1.0,
	touchEnabled: true,
	enabled: true,
	horizontalWrap: true,
	zIndex: 0,

	//##### DISPARITIES #####//

	// Setting to "null" on android works the first time. Leaves the color
	// on subsequent calls.
	backgroundColor: OS_ANDROID ? 'transparent' : null,

	// creates a font slightly different (smaller) than default on iOS
	// https://jira.appcelerator.org/browse/TIMOB-14565
	font: null,

	// Throws an exception on Android if set to null. Works on other platforms.
	// https://jira.appcelerator.org/browse/TIMOB-14566
	visible: true,

	// Setting to "null" on android makes text transparent
	// https://jira.appcelerator.org/browse/TIMOB-14567
	color: OS_ANDROID ? '#000' : null,

	// Android will leave artifact of previous transform unless the identity matrix is
	// manually reset.
	// https://jira.appcelerator.org/browse/TIMOB-14568
	//
	// Mobileweb does not respect matrix properties set in the constructor, despite the
	// documentation at docs.appcelerator.com indicating that it should.
	// https://jira.appcelerator.org/browse/TIMOB-14570
	transform: OS_ANDROID ? IDENTITY_TRANSFORM : null,

	// Crashes if set to null on anything but Android
	// https://jira.appcelerator.org/browse/TIMOB-14571
	backgroundGradient: !OS_ANDROID ? {} : null,

	// All supported platforms have varying behavior with border properties
	// https://jira.appcelerator.org/browse/TIMOB-14573
	borderColor: OS_ANDROID ? null : 'transparent',

	// https://jira.appcelerator.org/browse/TIMOB-14575
	borderRadius: OS_IOS ? 0 : null,

	// https://jira.appcelerator.org/browse/TIMOB-14574
	borderWidth: OS_IOS ? 0 : null
	};

	if (OS_IOS) {
		RESET = $tl._.extend(RESET, {
			backgroundLeftCap: 0,
			backgroundTopCap: 0
		});
	} else if (OS_ANDROID) {
		RESET = $tl._.extend(RESET, {
			backgroundDisabledColor: null,
			backgroundDisabledImage: null,
			backgroundFocusedColor: null,
			backgroundFocusedImage: null,
			focusable: false,
			keepScreenOn: false
		});
	}

  
  phone.phone_width_1 = {width: "8.333%" };
  phone.phone_width_2 = {width: "16.666%" };
  phone.phone_width_3 = {width: "25%" };
  phone.phone_width_4 = {width: "33.333%" };
  phone.phone_width_5 = {width: "41.666%" };
  phone.phone_width_6 = {width: "50%" };
  phone.phone_width_7 = {width: "58.333%" };
  phone.phone_width_8 = {width: "66.666%" };
  phone.phone_width_9 = {width: "75%" };
  phone.phone_width_10 = {width: "83.333%" };
  phone.phone_width_11 = {width: "91.666%" };
  phone.phone_width_12 = {width: "100%" };

  phone.phone_height_1 = {height: "10%" };
  phone.phone_height_2 = {height: "16.666%" };
  phone.phone_height_3 = {height: "25%" };
  phone.phone_height_4 = {height: "33.333%" };
  phone.phone_height_5 = {height: "41.666%" };
  phone.phone_height_6 = {height: "50%" };
  phone.phone_height_7 = {height: "58.333%" };
  phone.phone_height_8 = {height: "66.666%" };
  phone.phone_height_9 = {height: "75%" };
  phone.phone_height_10 = {height: "83.333%" };
  phone.phone_height_11 = {height: "91.666%" };
  phone.phone_height_12 = {height: "100%" };
    
  phone.phone_portrait_only = {orientationModes : [Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT]};
  phone.phone_landscape_only = {orientationModes : [Ti.UI.LANDSCAPE_LEFT, Ti.UI.LANDSCAPE_RIGHT]};
  phone.phone_hide = { visible:false, height:"0dp", width:"0dp"};
  
  phone.phone_icon_48 = {width:"48dp", height:"48dp",};
  phone.phone_icon_64 = {width:"64dp", height:"64dp",};
  phone.phone_icon_96 = {width:"96dp", height:"96dp",};
  phone.phone_icon_128 = {width:"128dp", height:"128dp",};
  phone.phone_icon_256 = {width:"256dp", height:"256dp",};
  phone.phone_icon_512 = {width:"512dp", height:"512dp",};
  
  tablet.tablet_width_1 = {width: "8.333%" };
  tablet.tablet_width_2 = {width: "16.666%" };
  tablet.tablet_width_3 = {width: "25%" };
  tablet.tablet_width_4 = {width: "33.333%" };
  tablet.tablet_width_5 = {width: "41.666%" };
  tablet.tablet_width_6 = {width: "50%" };
  tablet.tablet_width_7 = {width: "58.333%" };
  tablet.tablet_width_8 = {width: "66.666%" };
  tablet.tablet_width_9 = {width: "75%" };
  tablet.tablet_width_10 = {width: "83.333%" };
  tablet.tablet_width_11 = {width: "91.666%" };
  tablet.tablet_width_12 = {width: "100%" };
  
  tablet.tablet_height_1 = {height: "8.333%" };
  tablet.tablet_height_2 = {height: "16.666%" };
  tablet.tablet_height_3 = {height: "25%" };
  tablet.tablet_height_4 = {height: "33.333%" };
  tablet.tablet_height_5 = {height: "41.666%" };
  tablet.tablet_height_6 = {height: "50%" };
  tablet.tablet_height_7 = {height: "58.333%" };
  tablet.tablet_height_8 = {height: "66.666%" };
  tablet.tablet_height_9 = {height: "75%" };
  tablet.tablet_height_10 = {height: "83.333%" };
  tablet.tablet_height_11 = {height: "91.666%" };
  tablet.tablet_height_12 = {height: "100%" };
  
  tablet.tablet_icon_48 = {width:"48dp", height:"48dp",};
  tablet.tablet_icon_64 = {width:"64dp", height:"64dp",};
  tablet.tablet_icon_96 = {width:"96dp", height:"96dp",};
  tablet.tablet_icon_128 = {width:"128dp", height:"128dp",};
  tablet.tablet_icon_256 = {width:"256dp", height:"256dp",};
  tablet.tablet_icon_512 = {width:"512dp", height:"512dp",};
  
  
  tablet.tablet_portrait_only = {orientationModes : [Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT]};
  tablet.tablet_landscape_only = {orientationModes : [Ti.UI.LANDSCAPE_LEFT, Ti.UI.LANDSCAPE_RIGHT]};
  tablet.tablet_all_orientations = {orientationModes : [Ti.UI.LANDSCAPE_LEFT, Ti.UI.LANDSCAPE_RIGHT, Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT ]};
  tablet.tablet_hide = { visible:false, height:"0dp", width:"0dp"};
  tablet.tablet_show = { visible:true};

  tablet_landscape.tablet_l_width_1 = {width: "8.333%" };
  tablet_landscape.tablet_l_width_2 = {width: "16.666%" };
  tablet_landscape.tablet_l_width_3 = {width: "25%" };
  tablet_landscape.tablet_l_width_4 = {width: "33.333%" };
  tablet_landscape.tablet_l_width_5 = {width: "41.666%" };
  tablet_landscape.tablet_l_width_6 = {width: "50%" };
  tablet_landscape.tablet_l_width_7 = {width: "58.333%" };
  tablet_landscape.tablet_l_width_8 = {width: "66.666%" };
  tablet_landscape.tablet_l_width_9 = {width: "75%" };
  tablet_landscape.tablet_l_width_10 = {width: "83.333%" };
  tablet_landscape.tablet_l_width_11 = {width: "91.666%" };
  tablet_landscape.tablet_l_width_12 = {width: "100%" };
  
  tablet_landscape.tablet_l_hide = { visible:false, height:"0dp", width:"0dp"};

  
  tablet_portrait.tablet_p_width_1 = {width: "8.333%" };
  tablet_portrait.tablet_p_width_2 = {width: "16.666%" };
  tablet_portrait.tablet_p_width_3 = {width: "25%" };
  tablet_portrait.tablet_p_width_4 = {width: "33.333%" };
  tablet_portrait.tablet_p_width_5 = {width: "41.666%" };
  tablet_portrait.tablet_p_width_6 = {width: "50%" };
  tablet_portrait.tablet_p_width_7 = {width: "58.333%" };
  tablet_portrait.tablet_p_width_8 = {width: "66.666%" };
  tablet_portrait.tablet_p_width_9 = {width: "75%" };
  tablet_portrait.tablet_p_width_10 = {width: "83.333%" };
  tablet_portrait.tablet_p_width_11 = {width: "91.666%" };
  tablet_portrait.tablet_p_width_12 = {width: "100%" };
  
  tablet_portrait.tablet_p_hide = { visible:false, height:"0dp", width:"0dp"};
 
  //############### tilego components / windows styles  ##########
  general.hr = {
  	height: "2dp",
  	backgroundColor:"#ecf0f1"
  };
  general.window = { navBarHidden: true, fullscreen:true,};
  general.container = { height:"auto", width:"auto"};
  
  general.title = { font :{fontSize : 18,fontFamily : "OpenSans-Regular.ttf"}, color:"#ffffff", height: "25dp",}; 
  
  
  
  	//FormWidget
  	general.formWidget_default_container = {
  		width: "auto",
  		height: Ti.UI.SIZE,
  		layout: "horizontal"};
	
	general.formWidget_default_inputField = {
		color : '#336699',
		width : "200dp",
		height : "40dp",
		autocorrect : false,
		keyboardType : Ti.UI.KEYBOARD_DEFAULT,
		returnKeyType : Ti.UI.RETURNKEY_DEFAULT,
		borderStyle : Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
		borderColor: "transparent"};
		
	general.formWidget_default_label = {
		width: "auto",
  		color:	"#2c3e50",
		font : {fontSize : 12,fontFamily : "OpenSans-Regular.ttf"},};
	
	general.formWidget_error_inputField = {
		borderColor:"#EF4836"};
	
		
	general.formWidget_error_label = {
		left: "15dp",
		color:	"#EF4836",
		font : {fontSize : 14,fontFamily : "OpenSans-Regular.ttf"},};
  	
  	//####FormComponent
  	
  	//### DateWidget
  	phone.dateWidget_container = {layout:"vertical", height:"30%", width: "40%", backgroundColor:"transparent",borderColor:"#808080", borderRadius:"4dp" };
  	phone.dateWidget_monthLabel = { color:"#ffffff", font : {fontSize : 14,fontFamily : "OpenSans-Medium.ttf"},};
	phone.dateWidget_dayLabel = { color:"#ffffff", font : {fontSize : 14,fontFamily : "OpenSans-Medium.ttf"},};
	phone.dateWidget_dateLabel = { color:"#ffffff", font : {fontSize : 24,fontFamily : "OpenSans-Medium.ttf"},};
	
	tablet.dateWidget_container = {viewShadowOffset:{x:0,y:5},viewShadowRadius:2,viewShadowColor:"#d3d3d3", backgroundColor:"#3DBAD9",layout:"vertical",height:"30%", width: "20%", borderColor:"#808080", borderRadius:"4dp" };
  	tablet.dateWidget_monthLabel = { color:"#ffffff", font : {fontSize : 28,fontFamily : "Roboto-Thin.ttf"},};
	tablet.dateWidget_dayLabel = { top:"5%",color:"#ffffff", font : {fontSize : 28,fontFamily : "Roboto-Thin.ttf"},};
	tablet.dateWidget_dateLabel = { color:"#ffffff", font : {fontSize : 84,fontFamily : "Roboto-Thin.ttf"},};  	
  	
  //############### user defined styles  ##########
  phone.phone_menuItem = {height:"18%", };
  tablet.tablet_menuItem = {height:"100dp", width:"110dp"}; 
  
  var computedStyles = {};
/**
 * builds styles based on formfactor. phone will have only general and phone styles whereas
 * tablet will have tablet styles and styles associated with orientation along with general and phone
 *
 * @method buildStyles
 */
  var buildStyles = function(){
  	logger.debug("buidling style object");
  	computedStyles = {};
  	computedStyles = $tl._.extend(computedStyles,general,phone);
  	if($tl.device.isTablet()){
  		if(Ti.Gesture.isPortrait()){
  			computedStyles = $tl._.extend(computedStyles,tablet,tablet_portrait);
  		}
  		if(Ti.Gesture.isLandscape()){
  			computedStyles = $tl._.extend(computedStyles,tablet,tablet_landscape);
  		}
  	}
  	return computedStyles;
  };
  computedStyles = buildStyles();
 
  /**
 * resets the current style dictionary and rebuilds it. 
 * Usually used when orientation changes so that new styles are compiled
 *
 * @method resetStyles
 */
  var resetStyles = function(_args){
  	computedStyles = buildStyles();
  };
  
 /**
 * function to return styles computed based on input tss selectors.
 * It performs in-order merge, so the last source will override properties of the same name
 * styles in previous selector in the array. 
 * @method getStyle
 * @param {Array} of tss selectors
 * @return {Dictionary} of merged stlyes
 */
  var getStyle = function(_args){
  	if(_args){
  		var mergedStyle = {};
  		logger.debug("styles: "+_args);
  		for(var index = 0; index < _args.length; index++) {
        	mergedStyle = $tl._.extend(mergedStyle,computedStyles[_args[index]]);
  		}
  		//logger.debug("_args: "+ _args + " : " + JSON.stringify(mergedStyle));
  		return mergedStyle;
  	}
  };
  /**
   * @property delemiter ('|')
   */
  var delimiter = '|';
  var styleProperty = "tssStyle";
  return {
    resetStyles: resetStyles,
    getStyle:getStyle,
    delimiter:delimiter,
    styleProperty:styleProperty,
    RESET:RESET,
  };

})();
exports.resetStyles = ResponsiveStyle.resetStyles;
exports.getStyle = ResponsiveStyle.getStyle;
exports.delimiter = ResponsiveStyle.delimiter;
exports.styleProperty = ResponsiveStyle.styleProperty;
exports.RESET = ResponsiveStyle.RESET;
