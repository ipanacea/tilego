var baseComponent = require('tilego/components/base_component');


var Table = function(_args){
	if(_args){
		if(!_args.hasOwnProperty("name")){
			_args["name"] = "table";
		}	
	};
	baseComponent.call(this,_args);
	this.originalArgs = _args;
	this.init();
	this.onlySingleRowExpansion = this.originalArgs.onlySingleRowExpansion;
	this.lastInsertedIndex = null;	
	this.insertedRowsCount = null;	
};

Table.prototype = Object.create(baseComponent.prototype);
Table.prototype.constructor = Table;

Table.prototype.init = function(_args){
	var that = this;
	if(_args)
		this.component = Titanium.UI.createTableView(_args);
	else
		this.component = Titanium.UI.createTableView(that.originalArgs);
	//this.rowData = [];
};

Table.prototype.setData = function(_args){
		var that = this;
		//this.rowData = _args.rowData;
		this.component.setData(_args.rowData);
};

Table.prototype.resetData = function(_args){
	try{
				if(this.Util.Device.isAndroid()){
					this.reInit();
				}
				else{
					Ti.API.debug("Destroying tablerows # "+this.component.data.length);
    				for (var i = this.component.data[0].rows.length-1; i >= 0; i--) {
        				this.component.deleteRow(i);
    				}
    				this.component.setData([]);
    				//this.rowData = [];
    			}
			}
		
		catch(error){
			Ti.API.error("Error while resetting component:"+this.name);
		}	
};

Table.prototype.reInit = function(_args){
	if(_args)
		this.component = Titanium.UI.createTableView(_args);
	else{
		var that = this;
		this.component = Titanium.UI.createTableView(that.originalArgs);
		this.rowData = [];
	}	
};

Table.prototype.createRow = function(_args){
	var tableRow = Ti.UI.createTableViewRow(_args.data);
	if(_args.data){
		if(_args.data.selectedColor === "none" && _args.data.backgroundSelectedColor === "none"){
			if(this.Util.Device.properties.osname !== "android"){
				tableRow.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
			}
		}	
	}
	
	return tableRow;
};


Table.prototype.createSection = function(_args){
	var section = Ti.UI.createTableViewSection(_args.data);
	return section;
};


Table.prototype.insertRowAfter = function(_args){
	try{
		if(_args.row){
			if(this.Util.Device.properties.osname  === 'android'){
				this.component.insertRowAfter(_args.index,_args.row);
			}else{
				this.component.insertRowAfter(_args.index,_args.row,{animationStyle:Titanium.UI.iPhone.RowAnimationStyle.LEFT});
			}	
		}
		else
			Ti.API.warn("Table Component: invalid args");	
	}catch(error){
		Ti.API.error("Table Component: error inserting row after index:"+_args.index+ " error: "+error);
	}
};

Table.prototype.insertRowsAfter = function(_args){
	if(_args.rows instanceof Array ){
		if(_args.rows.length){
			_args.rows = _args.rows.reverse();
			this.lastInsertedIndex = parseInt(_args.index) + 1;
			this.insertedRowsCount = _args.rows.length;
			for(index in _args.rows){
				this.insertRowAfter({index:_args.index,row:_args.rows[index]});
			}
		}
	}
};

Table.prototype.insertRowBefore = function(_args){
	try{
		if(_args.row){ 
			if(this.Util.Device.properties.osname  === 'android'){
				this.component.insertRowBefore(_args.index,_args.row);
			}else{
				this.component.insertRowBefore(_args.index,_args.row,{animationStyle:Titanium.UI.iPhone.RowAnimationStyle.LEFT});
			}	
		}else
			Ti.API.warn("Table Component: invalid args");	
	}catch(error){
		Ti.API.error("Table Component: error inserting row before index:"+_args.index);
	}
};

Table.prototype.insertRowsBefore = function(_args){
	if(_args.rows instanceof Array){
		if(_args.rows.length){
			this.lastInsertedIndex = parseInt(_args.index) > 0 ? (parseInt(_args.index) - 1) : parseInt(_args.index) ;
			this.insertedRowsCount = _args.rows.length;
			_args.rows = _args.rows.reverse();
			for(index in _args.rows){
				this.insertRowBefore({index:_args.index,row:_args.rows[index]});
			}
		}
	}
};
Table.prototype.deleteRow = function(_args){
	if(_args.index){
		try{
			if(this.Util.Device.properties.osname  === 'android'){
			this.component.deleteRow(_args.index);
			}else{
				this.component.deleteRow(_args.index,{animationStyle:Titanium.UI.iPhone.RowAnimationStyle.RIGHT});
			}
		}catch(error){
			Ti.API.error("Table Component: error deleting row at index:"+_args.index);
		}
		
	}
};

Table.prototype.deleteRows = function(_args){
	if(_args.index && _args.count){
		Ti.API.info("Deleting rows insert at index: "+_args.index + " rows: "+_args.count);
		for(var i=0;i<_args.count; i++){
			this.deleteRow({index:_args.index});
		}
	}
};

Table.prototype.deleteLastInsert = function(_args){
	Ti.API.info("Deleting last insert at index: "+this.lastInsertedIndex + " rows: "+this.insertedRowsCount);
	if(this.lastInsertedIndex >= 0){
		this.deleteRows({index:this.lastInsertedIndex,count:this.insertedRowsCount});
	}
	this.lastInsertedIndex = null;
	this.insertedRowsCount = null;
};
module.exports=Table;

