var baseComponent = require('tilego/components/base_component');

/**
 * FormWidget class which is used by the {{#crossLink "FormComponent"}}{{/crossLink}}.
 * this creates a widget which has
 *		1) label  - label for the formfield
 * 		2) icon  - icon for the form field
 * 		3) inputfield - input form field
 * 		4) name - name of the form field, used while extracting data for sending it to server
 * @class FormWidget
 * @constructor
 * @extends BaseComponent
 * @module tilego
 * @submodule components
 */
var FormWidget = function(_args) {
	if (_args) {
		if (_args.hasOwnProperty("inputField")) {
			var inputField = _args["inputField"];
			if (inputField.hasOwnProperty("name") && inputField.hasOwnProperty("type")) {
				this.name = inputField["name"];
				baseComponent.call(this, {
					name : inputField["name"]
				});
				this.label = null;
				this.icon = null;
				this.inputfield = null;
				this.defaultStyle = {};
				this.defaultStyle.container = _args.container ? _args.container.properties : FormWidget.defaultStyle.container;
				if (_args.label) {
					if (_args.label.text)
						this.defaultStyle.label = this.Util._.extend(this.defaultStyle.label, FormWidget.defaultStyle.label, _args.label.properties);
				}
				this.defaultStyle.icon = _args.icon ? _args.icon.properties : FormWidget.defaultStyle.icon;
				this.defaultStyle.inputField = inputField.properties || FormWidget.defaultStyle.inputField;
				this.defaultStyle.inputField.name = this.name;
				this.validationRule = inputField.rules;
				this.type = inputField.type;
				this.errorLabel = null;
				this.didFailValidation = false;
				return;
			}
		}
	}
	throw "FormWidget requires argument with {inputField:{name : <name> , type: <type>} property defined";
};

FormWidget.prototype = Object.create(baseComponent.prototype);
FormWidget.prototype.constructor = FormWidget;
FormWidget.fieldType = {};
FormWidget.fieldType.TEXTFIELD = 'textField';
FormWidget.fieldType.TEXTAREA = 'textArea';
FormWidget.fieldType.SLIDER = 'slider';
FormWidget.fieldType.SWITCH = 'switch';
FormWidget.fieldType.PICKER = 'picker';

FormWidget.defaultStyle = {
	container : {
		tssStyle : "formWidget_default_container"
	},
	inputField : {
		tssStyle : "formWidget_default_inputField"
	},
	label : {
		tssStyle : "formWidget_default_label"
	},
};

FormWidget.validationErrorStyle = {
	inputField : {
		tssStyle : "formWidget_error_inputField"
	},
	errorLabel : {
		tssStyle : "formWidget_error_label"
	},
};
/**
 * @method getRule
 * @return {Ti.UI.View}
 */
FormWidget.prototype.getRule = function() {
	return this.validationRules;
};

/**
 * Validates the current form field
 * @method validate
 */
FormWidget.prototype.validate = function(_args) {
	var result = this.Util.Validate.validateField(this.inputfield, this.validationRule);
	if (!result.status) {
		this.didFailValidation = true;
	}
	// if it failed validation previously, but current check passed , then it will reset the style on the widget.
	if (this.didFailValidation && result.status) {
		this.resetStyle();
		this.didFailValidation = false;
	}
	return result;
};

/**
 * this method is called when validation fails and it updates based on setValidationError Style or defaults to style
 * set by the FormWidget.validationErrorStyle
 * @method validationFailureCallback
 */
FormWidget.prototype.validationFailureCallback = function(message) {
	var tempStyle = this.onValidationErrorStyle || FormWidget.validationErrorStyle;
	this.updateInputField(tempStyle.inputField);
	this.updateIcon(tempStyle.icon);
	this.updateLabel(tempStyle.label);
	if (!this.errorLabel) {
		var that = this;
		this.errorLabel = Ti.UI.createLabel();
		$tl.addStyle(that.errorLabel, tempStyle.errorLabel.tssStyle);
		this.errorLabel.setText(message);
		this.getComponent().add(that.errorLabel);
	}
};
/**
 * Method to reset the value in the inputField
 * @method reset
 */
FormWidget.prototype.reset = function() {
	this.getInputField().setValue('');
};

/**
 * Method to reset the style of the widget
 * @method reset
 */
FormWidget.prototype.resetStyle = function() {
	var that = this;
	if (this.errorLabel) {
		this.getComponent().remove(that.errorLabel);
		this.errorLabel = null;
	}
	this.updateInputField({borderColor:"transparent"});
	this.updateInputField(this.defaultStyle.inputField);
	this.updateIcon(this.defaultStyle.icon);
	this.updateLabel(this.defaultStyle.label);
	this.updateWidgetContainer(this.defaultStyle.container);
};
/**
 * updates the FormWidget container properties
 * @method updateComponent
 */
FormWidget.prototype.updateWidgetContainer = function(properties) {
	updateElement(this.getComponent(), properties);
};
/**
 * creates a view and sets the image property as backgroundImage
 * @method createIcon
 */
FormWidget.prototype.createIcon = function(properties) {
	if (properties) {
		this.icon = Ti.UI.createView(properties);
		this.icon.setBackgroundImage(properties.image);
	}
};

/**
 * @method getIcon
 * @return {Ti.UI.View}
 */
FormWidget.prototype.getIcon = function() {
	return this.icon;
};

/**
 * @method updateIcon
 */
FormWidget.prototype.updateIcon = function(properties) {
	if(properties){
		updateElement(this.getIcon(), properties);
		if(properties.hasOwnProperty("tssStyle")){
			var that = this;
			$tl.addStyle(that.getIcon(),properties["tssStyle"]);
		}
	}
};

/**
 * @method createLabel
 */
FormWidget.prototype.createLabel = function(properties) {
	if (properties) {
		this.label = Ti.UI.createLabel(properties);
	}
};

/**
 * @method getLabel
 * @return {Ti.UI.Label}
 */
FormWidget.prototype.getLabel = function() {
	return this.label;
};

/**
 * @method updateLabel
 */
FormWidget.prototype.updateLabel = function(properties) {
	if(properties){
		updateElement(this.getLabel(), properties);
		if(properties.hasOwnProperty("tssStyle")){
			var that = this;
			$tl.addStyle(that.getLabel(),properties["tssStyle"]);
		}
	}
	
};

/**
 * @method createInputField
 */
FormWidget.prototype.createInputField = function(type, properties) {
	if (type && properties) {
		switch (type) {
		case FormWidget.fieldType.TEXTFIELD:
			this.inputfield = Ti.UI.createTextField(properties);
			return true;
		case FormWidget.fieldType.TEXTAREA:
			this.inputfield = Ti.UI.createTextArea(properties);
			return true;
		case FormWidget.fieldType.SLIDER:
			this.inputfield = Ti.UI.createSlider(properties);
			return true;
		case FormWidget.fieldType.SWITCH:
			this.inputfield = Ti.UI.createSwitch(properties);
			return true;
		case FormWidget.fieldType.PICKER:
			this.inputfield = Ti.UI.createPicker(properties);
			return true;
		default:
			Ti.API.warn("unknown field type: " + type);
			return false;
		}
	}
};

/**
 * @method getInputField
 * @return {inputField} Ti.UI input fields
 */
FormWidget.prototype.getInputField = function() {
	return this.inputfield;
};

/**
 * @method updateInputField
 */
FormWidget.prototype.updateInputField = function(properties) {
	if(properties){
		updateElement(this.getInputField(), properties);
		if(properties.hasOwnProperty("tssStyle")){
			var that = this;
			$tl.addStyle(that.getInputField(),properties["tssStyle"]);
		}
	}
};

/**
 * @method setOnValidationErrorStyle
 */
FormWidget.prototype.setOnValidationErrorStyle = function(properties) {
	if (properties) {
		this.onValidationErrorStyle = properties;
	}
};

/**
 * @method setOnValidStyle
 * @param {Object} containing 'style' property
 */
FormWidget.prototype.setOnValidStyle = function(properties) {
	if (properties) {
		this.onValidStyle = properties;
	}
};

/**
 * @method setDefaultStyle
 * @param {Object} containing 'style' property
 */
FormWidget.prototype.setDefaultStyle = function(properties) {
	if (properties) {
		this.defaultStyle = properties;
	}
};

/**
 * @method getValue
 * @return {String} Value of the inputField
 */
FormWidget.prototype.getValue = function() {
	return this.getInputField().getValue();
};

/**
 * Builds the formWidget
 * @method build
 */
FormWidget.prototype.build = function(_args) {
	var that = this;
	this.containerView = this.component = Ti.UI.createView(that.defaultStyle.container);
	this.createInputField(this.type, this.defaultStyle.inputField);
	this.createIcon(this.defaultStyle.icon);
	this.createLabel(this.defaultStyle.label);
	//checking if we created a label as label is optional
	if (this.getLabel())
		this.component.add(that.getLabel());
	this.component.add(that.getInputField());
	
	//checking if we created an icon as icon is optional
	if (this.getIcon())
		this.component.add(that.getIcon());
	return this.getComponent();
};

FormWidget.prototype.destroy = function(_args) {
	this.defaultStyle = null;
	this.validationRule = null;
	this.type = null;
	this.errorLabel = null;
	this.didFailValidation = null;
	this.onValidationErrorStyle = null;
	this.onValidStyle = null;
	this.call_base(this, "destroy", _args);
};
//private functions
function updateElement(element, params) {
	if (element && params) {
		for (var key in params) {
			if (params.hasOwnProperty(key)) {
				element[key] = params[key];
			}
		}
	}
	return element;
}

/**
 * The FormComponent class which can be used to build forms. Provides utils for validation and error handling
 *
 * @class FormComponent
 * @constructor
 * @extends BaseComponent
 * @module tilego
 * @submodule components
 */
var FormComponent = function(_args) {
	if(_args){
		if(!_args.hasOwnProperty("name")){
			_args["name"] = "calendar";
		}	
	};
	baseComponent.call(this, _args);
	this.containerView = this.callback = _args.callback || FormComponent.callback;
	this.component = Ti.UI.createView(_args.container);
	this._formWidgetArray = [];
};
FormComponent.prototype = Object.create(baseComponent.prototype);
FormComponent.prototype.constructor = FormComponent;

FormComponent.callback = function(validationFailedFormWidgets) {
	while (validationFailedFormWidgets.length > 0) {
		var tempObj = validationFailedFormWidgets.pop();
		var errorMessage = tempObj.message;
		tempObj.formWidget.validationFailureCallback(errorMessage);
	}
};
FormComponent.fieldType = FormWidget.fieldType;

FormComponent.prototype.setOnValidationErrorStyle = function(_args) {
};

FormComponent.prototype.serialize = function() {
};

FormComponent.prototype.serializaArray = function() {
};

FormComponent.prototype.setOnValidStyle = function(_args) {
};

FormComponent.prototype.setlayout = function(_args) {
};

FormComponent.prototype.createFormFromJson = function(_args) {
};

FormComponent.prototype.validate = function(_args) {
	var errorWidgets = [];
	for (var index = 0; index < this._formWidgetArray.length; index++) {
		result = this._formWidgetArray[index].validate();
		if (!result.status) {
			errorWidgets.push({
				formWidget : this._formWidgetArray[index],
				message : result.message,
				rule : this._formWidgetArray[index].getRule()
			});
		}
	}
	if ( typeof this.callback === 'function') {
		this.callback(errorWidgets);
	}
	return errorWidgets.length > 0 ? false : true ;
};

FormComponent.prototype.setlayout = function(_args) {
};

FormComponent.prototype.createInputWidget = function(_args) {
	if (_args) {
		var formWidget = new FormWidget(_args);
		return formWidget;
	}
};

FormComponent.prototype.addInputWidget = function(inputWidget) {
	if ( inputWidget instanceof FormWidget) {
		this._formWidgetArray.push(inputWidget);
		inputWidget.build();
		this.getComponent().add(inputWidget.getComponent());
	} else
		Ti.API.error("inputWidget should be an instance of formWidget");
};

FormComponent.prototype.removeInputWidget = function(inputWidget) {

};

FormComponent.prototype.add = function(view) {
	if (view) {
		this.getComponent().add(view);
	}
};

FormComponent.prototype.remove = function(view) {

};

FormComponent.prototype.reset = function(view) {
	for (var index = 0; index < this._formWidgetArray.length; index++) {
		this._formWidgetArray[index].reset();
		this._formWidgetArray[index].resetStyle();
	}
};
FormComponent.prototype.build = function(_args) {
	var that = this;
	for (var index = 0; index < this._formWidgetArray.length; index++) {
		this._formWidgetArray[index].build();
		this.getComponent().add(that._formWidgetArray[index].getComponent());
	}
	return this.getComponent();
};

module.exports = FormComponent; 