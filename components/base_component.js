
var Util = require('tilego/util/Util');
var responsiveStyle = require ('tilego/windows/responsive_style');

/**
* This is most basic base class which objectifies the titanium views as components
*
* @class BaseComponent
* @constructor
* @module tilego
* @submodule components
*/
function BaseComponent(_args){
	  
	  if(_args.hasOwnProperty("name")){
	  	this.name=_args.name;
	  }
	  else{
	  	throw "Component not initialized well!. set a name by passing {name:\" Component Name\"}";
	  	return;
	  }
	  /** @property containerView titanium view which acts as a container to the component
	  */
	  this.containerView = null;
	  
	  /** @property actual titanium component such as Table, ScrollView, ScrollableView etc 
	  */
	  this.component = null;
	  
	  /** @property Util collection of utils supported by tilego
	  */
	  this.Util = Util; 
	  
	  
}

/**
 * method to call parent's methods in javascript, similar to super in java
 *
 * @method call_base
 * @param {object} this pointer of the current object
 * @param {String} name of the parent method that needs to be called
 * @param {JSON/param} arguments to the caller function 
 */
BaseComponent.prototype.call_base = function(object, method, args) {
    // We get base object, first time it will be passed object,
    // but in case of multiple inheritance, it will be instance of parent objects.
    var base = object.hasOwnProperty('_call_base_reference') ? object._call_base_reference : object,
    // We get matching method, from current object,
    // this is a reference to define super method.
            object_current_method = base[method],
    // Temp object wo receive method definition.
            descriptor = null,
    // We define super function after founding current position.
            is_super = false,
    // Contain output data.
            output = null;
    while (base !== undefined) {
        // Get method info
        descriptor = Object.getOwnPropertyDescriptor(base, method);
        if (descriptor !== undefined) {
            // We search for current object method to define inherited part of chain.
            if (descriptor.value === object_current_method) {
                // Further loops will be considered as inherited function.
                is_super = true;
            }
            // We already have found current object method.
            else if (is_super === true) {
                // We need to pass original object to apply() as first argument,
                // this allow to keep original instance definition along all method
                // inheritance. But we also need to save reference to "base" who
                // contain parent class, it will be used into this function startup
                // to begin at the right chain position.
                object._call_base_reference = base;
                // Apply super method.
                output = descriptor.value.call(object, args);
                // Property have been used into super function if another
                // call_base() is launched. Reference is not useful anymore.
                delete object._call_base_reference;
                // Job is done.
                return output;
            }
        }
        // Iterate to the next parent inherited.
        base = Object.getPrototypeOf(base);
    }
};

/**
 * returns the name of the component
 *
 * @method getName
 * @return {String} name of the component/view
 */
BaseComponent.prototype.getName = function(){
		return this.name;
};

/**
 * method to set the name of the component
 *
 * @method setName
 * @param {String} name of the component/view
 */
BaseComponent.prototype.setName = function(name){
		this.name = name;
};

/**
 * returns the component in string format, currently it just returns the name of the component
 *
 * @method toString
 * @return {String} name of the component/view
 */
BaseComponent.prototype.toString = function(){
		return this.name;
};

/**
 * method to destroy the component, it recursives loops throughs all the views and destroys them.
 *  (removes all child views and sets them to null and eventually sets the parent to null)
 *
 * @method destroy
 */
BaseComponent.prototype.destroy = function(){
	$tl.destroy(this.containerView);
	
};


/**
 * returns the container view of the component
 *
 * @method getContainerView
 * @return {Ti.UI.View} [Ti.UI.View](http://docs.appcelerator.com/titanium/3.0/#!/api/Titanium.UI.View)
 */
BaseComponent.prototype.getContainerView = function(){
	return this.containerView;
};

/**
 * returns the actual titanium component which has been wrapped as commonJS module 
 *
 * @method getComponent
 * @return {Object} component
 */
BaseComponent.prototype.getComponent = function(){
	if(!this.component["name"])
		this.component["name"] = this.name;
	return this.component;
};

/**
 * method to set the data for the component/view
 * typically used for setting new data to views when the component is being re-used
 *
 * @method setData
 * @param {JSON/Object} arguments as JSON/Object
 */
BaseComponent.prototype.setData = function(_args){	
};

/**
 * method to set the reset the data in the component/view
 * typically this should be called before reusing the component
 * @method resetData
 * @param {JSON/Object} arguments as JSON/Object
 */
BaseComponent.prototype.resetData = function(_args){	
};

BaseComponent.prototype.reInit = function(_args){	
};


BaseComponent.prototype.getActivityIndicator=function(_args){
	var indicatorView = Ti.UI.createView({
		height: "50dp",
		width: "180dp",
		backgroundColor: '#4eb660',
		//borderRadius: "2dp",
		//borderColor:"black",
		opacity: 0.7
	});
	
	var style;
	if (this.Util.Device.properties.osname !== "android") {
  		style = Ti.UI.iPhone.ActivityIndicatorStyle.DARK;
	}
	else {
  		style = Ti.UI.ActivityIndicatorStyle.BIG_DARK;
	}

	var activityIndicator = Ti.UI.createActivityIndicator({
	  color: 'black',
	  font: {fontFamily:'OpenSans-Semibold', fontSize:16,},
	  message: 'Loading...',
	  style:style,
	});
	activityIndicator.show();
	indicatorView.add(activityIndicator);

	return indicatorView;
};

BaseComponent.prototype.generateError=function(_args){
	
	
};

BaseComponent.prototype.applyStyle = function(view,renderChildren){
		$tl.renderStyle(view,renderChildren);
};




module.exports=BaseComponent;