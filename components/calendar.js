var baseComponent = require('tilego/components/base_component');
var logger = logging.getLogger("calendar.js");

var cal_days_labels = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
var cal_days_full_labels = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
var cal_months_labels = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September','October', 'November', 'December'];
var daylabelHeight, daylabelWidth, daylabelHeight, dayLabelFont, daylabelFontSize;
var datelabelHeight, datelabelWidth, datelabelHeight, dateLabelFont, datelabelFontSize;
daylabelWidth = datelabelWidth = Math.floor(100/7)+"%";


Calendar = function(_args) {
	if(_args){
		if(!_args.hasOwnProperty("name")){
			_args["name"] = "calendar";
		}	
	};
	baseComponent.call(this,_args);
	this.component = Ti.UI.createView(_args);
	this.component.setLayout("vertical");
	this.monthTitle = Ti.UI.createLabel({
		width : '200dp',
		height : '24dp',
		textAlign : 'center',
		color : '#252A3A',
		font : {
			fontSize : 17,
			fontFamily:"OpenSans-Semibold.ttf", 
		},
	});
	var that = this;
	var calendarHeader = Ti.UI.createView({
		top : '0dp',
		width : "100%",
		height : '55dp',
		backgroundColor : '#ffffff',
		layout : 'vertical'
	});
	
	// Tool Bar - Day's
	var calendarDaysView = Ti.UI.createView({
		top : '2dp',
		width : "100%",
		height : '42dp',
		layout : 'horizontal',
		left : '-1dp'
	});
	for (var i = 0; i < cal_days_labels.length; i++) {
		var dayLabel = Ti.UI.createLabel({
			left : '0dp',
			height : '30dp',
			text : cal_days_labels[i].toUpperCase(),
			width : daylabelWidth,
			textAlign : 'center',
			font : {
				fontSize : 12,
				fontFamily:"OpenSans-Semibold.ttf",
			},
			color : '#252A3A'
		});
		calendarDaysView.add(dayLabel);
	}

	calendarHeader.add(that.monthTitle);
	calendarHeader.add(calendarDaysView);
	this.component.add(calendarHeader);
	
}; 


Calendar.prototype = Object.create(baseComponent.prototype);
Calendar.prototype.constructor = Calendar;


Calendar.prototype.setData = function(_args){	
	this.generateSwipableCalendarView(_args);
};

Calendar.prototype.resetData = function(_args){	
};

Calendar.prototype.reInit = function(_args){	
};


Calendar.prototype.dayView = function(_args) {
    var label = Ti.UI.createLabel({
        current : _args.current,
        width : datelabelWidth,
        height : '19%',
        backgroundColor : '#ffffff',
        text : _args.day,
        textAlign : 'center',
        color : _args.color,
		realDate: _args.date,
        font : {
            fontSize : 14,
            fontFamily:"OpenSans-Light.ttf", 
           // fontWeight : 'bold'
        }
    });
    return label;
};

Calendar.prototype.monthName = function(_args) {
    return cal_months_labels[_args];
};



Calendar.prototype.generateSwipableCalendarView = function(_args) {
	var that = this;
	var slideNext = Titanium.UI.createAnimation({
		duration : 500
	});
	slideNext.left = "-100%";

	var slideReset = Titanium.UI.createAnimation({
		duration : 500
	});

	slideReset.left = '-1';
	
	var slidePrev = Titanium.UI.createAnimation({
		duration : 500
	});

	slidePrev.left = "100%";
	
	var calView = function(year, month, date) {
		logger.info(year + " " + month + " " + date);
		var nameOfMonth = that.monthName(month);
		
		//create main calendar view
		var mainView = Ti.UI.createView({
			layout : 'horizontal',
			width : "100%",
			height : 'auto',
			top : '0dp'
		});

		//set the time
		var daysInMonth = 32 - new Date(year, month, 32).getDate();
		var dayOfMonth = new Date(year, month, date).getDate();
		var dayOfWeek = new Date(year, month, 1).getDay();
		var daysInLastMonth = 32 - new Date(year, parseInt(month) - 1, 32).getDate();
		var daysInNextMonth = (new Date(year, month, daysInMonth).getDay()) - 6;
		var oldDay;
		//set initial day number
		var dayNumber = daysInLastMonth - dayOfWeek + 1;

		//get last month's days
		for ( i = 0; i < dayOfWeek; i++) {
			mainView.add( that.dayView({
				day : dayNumber,
				color : '#8e959f',
				current : 'no',
				dayOfMonth : ''
			}));
			dayNumber++;
		}
		// reset day number for current month
		dayNumber = 1;

		//get this month's days
		//  THE CURRENT DATE

		for ( i = 0; i < daysInMonth; i++) {
			var newDay =  that.dayView({
				day : dayNumber,
				color : '#252A3A',
				current : 'yes',
				dayOfMonth : dayOfMonth
			});

			newDay.date = year + "" + month + "" + (i+1);
			newDay.dateString = new Date(year,month,(i+1)).toString();
			mainView.add(newDay);
			if (newDay.date == dateString) {
				newDay.color = '#E84F4F';
				newDay.backgroundColor = '#FFFFFF';
				
				//oldDay = newDay;
			}
			dayNumber++;
		}

		dayNumber = 1;
		//get remaining month's days
		for ( i = 0; i < daysInNextMonth; i--) {
			mainView.add( that.dayView({
				day : dayNumber,
				color : '#8e959f',
				current : 'no',
				dayOfMonth : ''
			}));
			dayNumber++;
		}

		// this is the new "clicker" function, although it doesn't have a name anymore, it just is.
		mainView.addEventListener('click', function(e) {
			if (e.source.current == 'yes') {
				// reset last selected day
				if (oldDay) {
						oldDay.color = '#252A3A';
						oldDay.backgroundColor = '#ffffff';
				}
				// set characteristic of the day selected
				
				e.source.backgroundColor = '#252A3A';
				e.source.color = 'white';
				oldDay = e.source;
				Ti.API.info("Calendar: clicked on date: "+e.source.dateString);
				if(_args.callbackEvent){
					Titanium.App.fireEvent(_args.callbackEvent,{dateString:e.source.dateString});
				}
			}
		});
		return mainView;
	};

	// what's today's date?
	var todayDate = new Date();
	var year  = todayDate.getFullYear();
	var month  = todayDate.getMonth();
	var day  = todayDate.getDate();

	var dateString = year + "" + month + "" + day;
	// add the three calendar views to the window for changing calendars with animation later

	var prevCalendarView = null;
	if (month == 0) {
		prevCalendarView = calView(year - 1, 11, day);
	} else {
		prevCalendarView = calView(year, month - 1, day);
	}
	prevCalendarView.left = "-100%";

	var nextCalendarView = null;
	if (month == 0) {
		nextCalendarView = calView(year + 1, 0, day);
	} else {
		nextCalendarView = calView(year, month + 1, day);
	}
	nextCalendarView.left = "100%";

	var thisCalendarView = calView(year, month, day);
	this.monthTitle.text = this.monthName(month) + ' ' + year;
	this.calendarContainerView = Ti.UI.createView({
		height:"80%",
		width:"100%",
		top:"10dp"
	});
	this.calendarContainerView.add(thisCalendarView);
	this.calendarContainerView.add(nextCalendarView);
	this.calendarContainerView.add(prevCalendarView);
	// adding swipe event listener to calanderdarContainer
	this.calendarContainerView.addEventListener('swipe', function(e) {
			logger.debug("calendar swiped");
			if (e.direction === "left") {
				if (month == 11) {
					month = 0;
					year++;
				} else {
					month = month + 1;
				}

				//thisCalendarView.animate(slideNext);
				//nextCalendarView.animate(slideReset);

				setTimeout(function() {
					thisCalendarView.left = "-100%";
					nextCalendarView.left = '-1dp';
					
					prevCalendarView = thisCalendarView;
					thisCalendarView = nextCalendarView;
					
					if (month == 11) {
						nextCalendarView = calView(year + 1, 0, day);
					} else {
						nextCalendarView = calView(year, month +1, day);
					}
					that.monthTitle.text = that.monthName(month) + ' ' + year;
					console.log("swiped left to : "+year + " " + month + " " + day);
					nextCalendarView.left = "100%";
					that.calendarContainerView.add(nextCalendarView);
					
				}, 200);
			} else if (e.direction === "right") {
				if (month == 0) {
					month = 11;
					year--;
				} else {
					month = month - 1;
				}
				//thisCalendarView.animate(slidePrev);
				//prevCalendarView.animate(slideReset);
				setTimeout(function() {
					thisCalendarView.left = "100%";
					prevCalendarView.left = '-1dp';
					
					nextCalendarView = thisCalendarView;
					thisCalendarView = prevCalendarView;
					if (month == 0) {
						prevCalendarView = calView(year-1, 11, day);
					} else {
						prevCalendarView = calView(year, month -1 , day);
					}
					that.monthTitle.text = that.monthName(month) + ' ' + year;
					console.log("swiped right to : "+year + " " + month + " " + day);
					prevCalendarView.left = "-100%";
					that.calendarContainerView.add(prevCalendarView);
					//win.add(prevCalendarView);
				}, 200);
			}

		});
		
	
	
	
	this.component.add(that.calendarContainerView);
};





module.exports=Calendar;