/**
 * // Make a new grid with the dimensions we want
var grid = new TiGrid({
    height: 240,
    width: 320,
    cols: 4,
    rows: 5,
    margin: 4
});
```

### Coordinates within the grid
You may position anything within the grid by referencing some `x,y` coordinates from the grid. e.g.

|  -  |  -  |  -  |  B  |
| --- | --- | --- | --- |
|  -  |  -  |  -  |  -  |
|  -  |  -  |  -  |  -  |
|  -  |  -  |  -  |  -  |
|  A  |  -  |  -  |  -  |

Here `A is 0,0` and `B is 3,4`

Now, to retrieve a cells information from within the grid call the `grid.coord` method whilst passing in your x,y coordinates. This will return an object with height, width, left and bottom attributes, not overly usefull. 
With coordinates set, extend your call to include the `position` method and it will fill the cell at those coordinates with a view, perfectly positioned and sized.

```javascript
var myView = Ti.UI.createView();
grid.coord(0,0).position(myView);
```    

`myView` will be positioned to the lower left of the grid (`A` in our previous example). It's height and width will be set to fit neatly into that grid coordinate.

### colspan and rowspan
Since its unlikely you'll want everything the same size within your grid you can use the `colspan` and `rowspan` parameters to position your views into larger areas whilst still respecting any margins you've set.

```javascript
var myView = Ti.UI.createView();
// myView's height will stretch over 2 rows
grid.coord(0,0,{rowspan: 2}).position(myView);

// Or have your view stretch 2 columns and 2 rows
grid.coord(0,0,{rowspan: 2, colspan: 2}).position(myView);
```
 * 
 * 
 * 
 * 
 * 
 */






/**
 * TiGridCoordinate adds some sugar to TiGrid
 * 
 * @argument _args Object
 */
function TiGridCoordinate (/*Object*/ _args) {
	this.bottom = _args.bottom;
	this.top = _args.bottom + _args.height;
	this.left = _args.left;
	this.right = _args.left + _args.width;
	this.height = _args.height;
	this.width = _args.width;
}

/**
 * Set the size and position of a view to the 
 * confines and locaiton of a grid coordinate
 * 
 * @argument view TiView
 */
TiGridCoordinate.prototype.position = function (/*TiView*/ view) {
	view.bottom = this.bottom;
	view.left = this.left;
	view.height = this.height;
	view.width = this.width;
};

/**
 * TiGrid is a simple function to assist
 * with evenly distributed positioning of elements
 * 
 * @argument _args Object 
 *  cols: Number of grid columns, 
 *  rows: Number of grid rows, 
 *  width: Width of the grid
 *  height: Height of the grid
 *  margin: Amount of space to leave on all sides
 * }
 */
TiGrid = function(/*Object*/ _args) {
	_args = _args || {};
	
	this.cols = _args.cols || 1;
	this.rows = _args.rows || 1;
	this.width = _args.width || 0;
	this.height = _args.height || 0;
	this.margin = _args.margin || 0;
	
	// Rows, cols, height and width are all required
	if (this.cols < 1 || this.rows < 1 || this.width < 1 || this.height < 1) {
		throw new Error('Incorrect parameters: cols, rows, width and height should be defined and greater than zero');
	}
};

/**
 * Get the position and size of elements using grid coordinates
 * 
 * @argument x Int	Horizontal cell location starting at 0
 * @argument y Int	Vertical cell location starting at 0
 * @argument _args Object
 *  colspan: Number of cells the position stretches horizontally
 *  rowspan: Number of cells the position stretches vertically
 */
TiGrid.prototype.coord = function (x, y, /*Object*/ _args) {
	_args = _args || {};
	
	var colspan = _args.colspan || 1,
		rowspan = _args.rowspan || 1;
	
	// x and y must be within their bounds
	if (x < 0 || x > this.cols || y < 0 || y > this.rows) {
		throw new Error('Incorrect parameters: x or y out of bounds');
	}
	
	// colspan and rowspan must be within their bounds
	if (rowspan < 1 || rowspan > this.rows || colspan < 1 || colspan > this.cols) {
		throw new Error('Incorrect parameters: colspan or rowspan out of bounds');
	}
	
	// Finally return the position they were after
	return new TiGridCoordinate({
		bottom: ( (y * (this.height / this.rows) + this.margin)) + "dp",
		left: ((x * (this.width / this.cols) + this.margin)) + "dp",
		height: ((rowspan * Math.ceil(this.height / this.rows)) - (this.margin * 2)) + "dp",
		width: ((colspan * Math.ceil(this.width / this.cols)) - (this.margin * 2)) + "dp",
	});
};
var baseComponent = require ('tilego/components/base_component');


Grid = function(_args){
	if(_args){
		if(!_args.hasOwnProperty("name")){
			_args["name"] = "grid";
		}	
	};
	baseComponent.call(this,_args);
	this.originalArgs = _args;
	this.component = new TiGrid(_args);
};

Grid.prototype = Object.create(baseComponent.prototype);
Grid.prototype.constructor = Grid;


Grid.prototype.setData = function(_args){
	this.component.coord(_args.x,_args.y,{rowspan: _args.rowspan, colspan: _args.colspan}).position(_args.view);	
};

Grid.prototype.resetData = function(_args){	
};

Grid.prototype.reInit = function(_args){	
};





module.exports=Grid;


