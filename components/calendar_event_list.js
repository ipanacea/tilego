
var baseComponent = require ('tilego/components/table');

var hasAccessToCalendar = false;
var Util ;
var CalendarEventList = function(_args){
	if(_args){
		if(!_args.hasOwnProperty("name")){
			_args["name"] = "calendarEventList";
		}	
	};
	baseComponent.call(this,_args);
	Util = this.Util;
	if (this.Util.Device.isIOS()){
		if (Ti.Calendar.eventsAuthorization == Ti.Calendar.AUTHORIZATION_AUTHORIZED) {
        	Ti.API.info("Got access to calendar");
    	}else{
        	Ti.Calendar.requestEventsAuthorization(function(e){
            	if (e.success) {
                	hasAccessToCalendar = true;
                	
   					this.setData( this.setDataArgs);
            	}else {
                alert('Access to calendar is not allowed');
            	}
        	});
    	}
	}
};

CalendarEventList.prototype = Object.create(baseComponent.prototype);
CalendarEventList.prototype.constructor = CalendarEventList;


CalendarEventList.prototype.setData = function(_args){
		var that = this;
		this.setDataArgs = _args;
		/*if(hasAccessToCalendar){
			var eventMap = this.readDeviceCalendarInMonth(_args);
		}*/
		this.component.setData([]);
		this.component.setData(that.createEventRows({year:_args.year,month:_args.month,date:_args.date,test:true }));
};

CalendarEventList.prototype.resetData = function(_args){
		
};

CalendarEventList.prototype.reInit = function(_args){	
};

CalendarEventList.prototype.readDeviceCalendarInMonth = function(_args){
	var events_In_Month = new MonthEventsMap(_args);
	if(_args && _args.year &&_args.month){
		//selectableCalendars[0].id  selectableCalendars[0].name
    	var selectableCalendars = Ti.Calendar.allCalendars;
    	for(cal in selectableCalendars){
    		var selectedCalendarName = selectableCalendars[cal].name;
			var selectedid = selectableCalendars[cal].id;
			var calendar = Ti.Calendar.getCalendarById(selectedid);
			Ti.API.info('Calendar was of type' + calendar);
    		Ti.API.info('calendar that we are going to fetch is :: '+ calendar.id + ' name:' + calendar.name);
    		var events = calendar.getEventsInMonth(_args.year,_args.month);
    		for(index in events){
    			events_In_Month.addEvent(events[index]);
    		}
    	}
    	return events_In_Month.getEventMap();
	}
	else{
		Ti.API.error("Invalid args sent to readDeviceCalendarInMonth");
		return;
	}
};




CalendarEventList.prototype.readDeviceCalendarInDate = function(_args){
	var events_In_Date = new DayEventsMap(_args);
	if(_args && _args.year &&_args.month && _args.date){
		//selectableCalendars[0].id  selectableCalendars[0].name
    	var selectableCalendars = Ti.Calendar.allCalendars;
    	//for(cal in selectableCalendars){
    		Ti.Calendar.defaultCalendar;
    		var selectedCalendarName = selectableCalendars[0].name;
			var selectedid = selectableCalendars[0].id;
			//var calendar = Ti.Calendar.getCalendarById(selectedid);
			var calendar = Ti.Calendar.defaultCalendar;
			Ti.API.info('Calendar was of type' + calendar);
    		Ti.API.info('calendar that we are going to fetch is :: '+ calendar.id + ' name:' + calendar.name);
    		var events = calendar.getEventsInDate(_args.year,_args.month,_args.date);
    		//var events = calendar.getEventsInYear(_args.year);
    		for(index in events){
    			events_In_Date.addEvent(events[index]);
    		}
    	//}
    	Ti.API.info("Calendar: Events read: "+events.length + "date: "+_args.date);
    	return events_In_Date.getEventMap();
	}
	else{
		Ti.API.error("Invalid args sent to readDeviceCalendarInMonth");
		return;
	}
};


CalendarEventList.prototype.createEventRows = function(_args){
	var d1 = new Date();
	if(!_args.test){ 
		var dayEvents = this.readDeviceCalendarInDate(_args);
		var events = dayEvents[_args.date];
	}
	else{
		var events = [];
		for(var i= 0; i< 6; i++){
			var date1 = new Date(new Date().getTime() + 3000 +i *3600 *1000),
        	date2 = new Date(new Date().getTime() +i *3600 *1000 + 900000);
    		var event1 = {
                        title: 'Sample Event',
                        notes: 'Having dilated pupils and feeling awesome!So have to meet the doc!',
                        location: 'Srinagar Clinic',
                        begin: date1,
                        end: date2,
                        availability: Ti.Calendar.AVAILABILITY_FREE,
                        allDay: false,
                        visitType:"InPatient"
            };
   			events.push(event1);
		}
	}
	var eventRows = [];
	Ti.API.info(new Date().getTime() - d1.getTime());
	
	for(index in events){
		var evenTitle = events[index].title;
		var eventDate = this.Util.Moment(events[index].begin);
		var timeText = eventDate.format("hh:mm A");
			
		var tableRow = this.createRow({
				data:{
					height:"100dp",
					backgroundColor:"transparent",
					width:"100%",
					selectedColor:"none",
					backgroundSelectedColor:"none",
					auto:true,
					top:"0dp",
					dataObject:events[index],
					isEvent:true
				}
			});
			var rowView = Ti.UI.createView({
				height : "100%",
				backgroundColor : "#FFFFFF", //#F6F6F6
				width : "100%",
				//layout:"horizontal"
			});
			var timeLabel = Ti.UI.createLabel({
				text : timeText,
				left : "10dp",
				font : {
					fontSize : 11,
					fontFamily : "OpenSans-Bold.ttf"
				},
				color : "#56585E",
			});
			var eventDetailsContainer = Ti.UI.createView({
				height : "100%",
				backgroundColor : "#FFFFFF", //#F6F6F6
				width : "100%",
				left:"60dp"
				//layout:"vertical"
			});
			var eventTitle = Ti.UI.createLabel({
				text : evenTitle,
				left : "10dp",
				font : {
					fontSize : 18,
					fontFamily : "OpenSans-Regular.ttf"
				},
				color : "#56585E",
				top:"10dp",
				height:"25dp"
				
			});
			var notesLabel = Ti.UI.createLabel({
				text : events[index].notes,
				left : "10dp",
				font : {
					fontSize : 8,
					fontFamily : "OpenSans-Light.ttf"
				},
				color : "#56585E",
				height:"15dp",
				width:"80%",
				top:"30dp"
			});
			var locationAndVisitTypeContainer = Ti.UI.createView({
				height : "20dp",
				backgroundColor : "#FFFFFF", //#F6F6F6
				width : "100%",
				layout:"horizontal",
				bottom:"10dp"
			});
			var pinIcon = Ti.UI.createView({
				height:"10dp",
				width:"6dp",
				left: "10dp",
				backgroundImage:"/images/mapPin.png"
			});
			var locationLabel = Ti.UI.createLabel({
				text : events[index].location,
				left : "6dp",
				font : {
					fontSize : 9,
					fontFamily : "OpenSans-Regular.ttf"
				},
				color : "#56585E",
				height:"20dp",
				width:"40%"
				
			});
			var labelIcon = Ti.UI.createView({
				height:"16dp",
				width:"16dp",
				left: "10dp",
				backgroundImage:"/images/label.png"
			});
			var visitTypeLabel = Ti.UI.createLabel({
				text : events[index].visitType,
				left : "10dp",
				font : {
					fontSize : 9,
					fontFamily : "OpenSans-Regular.ttf"
				},
				color : "#56585E",
				height:"20dp",
				//width:"40%"
			});
			locationAndVisitTypeContainer.add(pinIcon);
			locationAndVisitTypeContainer.add(locationLabel);
			locationAndVisitTypeContainer.add(labelIcon);
			locationAndVisitTypeContainer.add(visitTypeLabel);
			eventDetailsContainer.add(eventTitle);
			eventDetailsContainer.add(notesLabel);
			eventDetailsContainer.add(locationAndVisitTypeContainer);
			rowView.add(timeLabel);
			rowView.add(eventDetailsContainer);
			tableRow.add(rowView);
			eventRows.push(tableRow);
	}
	Ti.API.info("Calendar: Number of events rows created: "+eventRows.length);
	return eventRows;
};
// private functions
MonthEventsMap = function(_args){
	this.monthEventsMap = {};
	this.month = _args.month;
	this.year = _args.year;
};
MonthEventsMap.prototype.addEvent = function(event){
	var eventDate = Util.Moment(event.getBegin());
	if(this.monthEventsMap.hasOwnProperty(eventDate.date())){
		this.monthEventsMap[eventDate.date()].push(event);
	}
	else{
		this.monthEventsMap[eventDate.date()] = [];
		this.monthEventsMap[eventDate.date()].push(event);
	}
};
MonthEventsMap.prototype.getEventMap = function(){
	//sorts based on start time in a day
	for(key in this.monthEventsMap){
		this.monthEventsMap[key] = this.monthEventsMap[key].sort(function(a, b) {
    		a = a.begin;
    		b = b.begin;
    		return a>b ? 1 : a<b ? -1 : 0;
		});
	}
	return this.monthEventsMap;
};	
DayEventsMap = function(_args){
	MonthEventsMap.call(this,_args);
	this.date = _args.date;
};
DayEventsMap.prototype = MonthEventsMap.prototype;
DayEventsMap.prototype.constructor = DayEventsMap;	

module.exports=CalendarEventList;