
var baseComponent = require ('tilego/components/base_component');
var theme = {};
theme.articleTiles = {
		defaults : {
			image : '/images/Default_Stadium_Image.png',
			title : "Title",
			height : "290dp",
		},
		view : {
			//image : '/images/gradient.jpg'
			color : '#bdc3c7',
			opacity : 1,
		},
		header : {
			font : "Roboto-Regular",
			fontSize : "18",
			fontColor : "white"
		},
		tile : {
			font : "Roboto-Medium",
			fontSize : "17",
			fontColor : "#3C3C3C"
		},
};

var maxImageHeight = 250; // in dp
var tileDescriptionHeight;
var ArticleTile = function(_args){
	this.originalArgs = _args;
	this.component = Ti.UI.createView(_args);
	var datePublished_top = 5;
	var title_top = 5;
	var category_top = 2;
	var subheadingFontSize = 12;
	var that= this;
	this.url = "";
	if(_args.shadow){
			var shadowView = Ti.UI.createView({
				width:"100%",
				height:"2dp",
				bottom:0,
				right:0,
				backgroundColor:"#a7a7a9",
			});
			this.component.add(shadowView);
	}
	// To contain image and title.
	var tileContainerView = Ti.UI.createView({
		backgroundColor : 'white',
		width : '100%',
		height : '99%',
		layout:"vertical"
	});
	this.imageContainer = Ti.UI.createView({
		height : '180dp',
		width : '100%',	
	});
	this.imageView = Ti.UI.createImageView({
		height : 'auto',
		width : 'auto',	
		top:"0"
	});
	this.imageContainer.add(that.imageView);
	this.datePublishedLabel = Ti.UI.createLabel({
		text : "",
		font : {
			fontFamily : "OpenSans-Regular",
			fontSize : subheadingFontSize,
		},
		color : "#7f8c8d",
		top : datePublished_top+'dp',
		left : '20dp',
	});
	
	this.titleLabel = Ti.UI.createLabel({
		text : "",
		font : {
			fontFamily : theme.articleTiles.tile.font,
			fontSize : theme.articleTiles.tile.fontSize,
		},
		color : theme.articleTiles.tile.fontColor,
		top : title_top+'dp',
		left : '20dp',
		visible : true,
		width:"90%",
		height:parseInt(theme.articleTiles.tile.fontSize)*3 + "dp" 
	});
	this.categoryLabel = Ti.UI.createLabel({
		text : "",
		font : {
			fontFamily : "OpenSans-Regular",
			fontSize : subheadingFontSize,
		},
		color : "#7f8c8d",
		top : category_top+'dp',
		right : '5dp',
	});
	tileDescriptionHeight = (datePublished_top+title_top+category_top + (subheadingFontSize *2) + parseInt(theme.articleTiles.tile.fontSize)*3 + 15);
	tileContainerView.add(that.imageContainer);
	tileContainerView.add(that.datePublishedLabel);
	tileContainerView.add(that.titleLabel);
	tileContainerView.add(that.categoryLabel);
	this.component.add(tileContainerView);
	this.containerView = this.component; 
};

ArticleTile.prototype = new baseComponent({name:"ArticleTile"});
ArticleTile.prototype.constructor = ArticleTile;


ArticleTile.prototype.setData = function(_args){
	Ti.API.debug(JSON.stringify(_args));
	if(this.isHidden){
		this.component.show();
	}
	this.resetData();
	this.url = _args.url;
	if (_args.image && _args.image.src !== "") {
				this.adjustImageAndTile({width:_args.image.width,height:_args.image.height});
				this.titleLabel.top = "5dp";
				this.imageView.image = _args.image.src;
	}else{
		this.imageView.image = null;
		this.imageContainer.height = "0dp";
		this.component.height = "115dp";
		this.titleLabel.top = "10dp";
	}
	this.titleLabel.text = _args.title;
	this.datePublishedLabel.text = this.Util.functions.getElapsedDurationInHours(_args.datePublished);
	this.categoryLabel.text = _args.category;
};

ArticleTile.prototype.resetData = function(_args){
	this.imageView.image = null;
	this.imageView.setImage(null);
	this.titleLabel.text = "";
	this.datePublishedLabel.text = "";
	this.categoryLabel.text = "";
	this.url = "";	
};

ArticleTile.prototype.reInit = function(_args){	
};

ArticleTile.prototype.hide = function(){
	this.isHidden = true;
	this.component.hide(); 
};

ArticleTile.prototype.adjustImageAndTile = function(_args){
	var width = parseInt(_args.width);
	var height = parseInt(_args.height);
	var desiredWidth = Math.floor(parseInt(this.originalArgs.width) * this.Util.Device.properties.displayWidth /100);
	var multiplyingFactor = desiredWidth/width;
	Ti.API.info("multiplying factor: " +multiplyingFactor);
	if(this.Util.Device.isIOS()){
			this.imageView.width = Math.ceil(width *multiplyingFactor) + "dp";
			this.imageView.height = Math.ceil(height *multiplyingFactor) + "dp";
			this.imageContainer.height = maxImageHeight > Math.ceil(height *multiplyingFactor) ? Math.ceil(height *multiplyingFactor) :maxImageHeight;
			this.component.height = this.imageContainer.height + tileDescriptionHeight + "dp";
			this.imageContainer.height = this.imageContainer.height + "dp";
	}else{
		// width given by android is in real pixels, convert to dp by dividing by density factor
		this.imageView.width = Math.ceil(width *multiplyingFactor /this.Util.Device.properties.displayLogicalDensityFactor ) + "dp";
		this.imageView.height = Math.ceil(height *multiplyingFactor / this.Util.Device.properties.displayLogicalDensityFactor) + "dp";
		this.imageContainer.height = maxImageHeight > Math.ceil(height *multiplyingFactor / this.Util.Device.properties.displayLogicalDensityFactor) ? Math.ceil(height *multiplyingFactor / this.Util.Device.properties.displayLogicalDensityFactor) :maxImageHeight;
		this.component.height = this.imageContainer.height + tileDescriptionHeight + "dp";
		this.imageContainer.height = this.imageContainer.height + "dp";
	}
	Ti.API.info("original width: "+width + " height: "+height +" adjusted width: "+this.imageView.width + " height: "+this.imageView.height + " component height: "+this.component.height);
};

ArticleTile.prototype.getURL = function(_args){
	return this.url;
};

//private functions
function determineImageDims(width,height){
	
}

module.exports=ArticleTile;