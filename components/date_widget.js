
var baseComponent = require('tilego/components/base_component');


var DateWidget = function(_args){
	if(_args){
		if(!_args.hasOwnProperty("name")){
			_args["name"] = "DateWidget";
		}	
	};
	baseComponent.call(this, _args);
	var currentDateTime = new this.Util.Moment();
	if(_args){
		var containerStyle = _args.container || _args.component || {tssStyle:"dateWidget_container"};
	}else{
		var containerStyle = {tssStyle:"dateWidget_container"};
	}
	this.containerView = this.component = Ti.UI.createView(containerStyle);
	this.monthLabel = Ti.UI.createLabel({
		tssStyle:"dateWidget_monthLabel",
		text:currentDateTime.format("MMMM")
	});
	this.dayLabel = Ti.UI.createLabel({
		tssStyle:"dateWidget_dayLabel",
		text:currentDateTime.format("dddd")
	});
	this.dateLabel = Ti.UI.createLabel({
		tssStyle:"dateWidget_dateLabel",
		text:currentDateTime.date()
	});
	var that = this;
	this.component.add(that.dayLabel);
	this.component.add(that.dateLabel);
	this.component.add(that.monthLabel);

};

DateWidget.prototype = Object.create(baseComponent.prototype);;
DateWidget.prototype.constructor = DateWidget;


DateWidget.prototype.setData = function(_args){	
};

DateWidget.prototype.resetData = function(_args){
	this.refresh();	
};

DateWidget.prototype.reInit = function(_args){
	this.refresh();	
};

DateWidget.prototype.refresh = function(_args){
	var currentDateTime = new this.Util.Moment();
	this.dayLabel.set(textcurrentDateTime.format("dddd"));
	this.monthLabel.setText(currentDateTime.format("MMMM"));
	this.dateLabel.setText(currentDateTime.date()); 
};



module.exports=DateWidget;