
var baseComponent = require ('tilego/components/BaseComponent');


var ScrollableViewWithHeaderMenu = function(_args){
	this.containerView = Ti.Ui.createView({
		
	});
	this.headerMenuViewWidth = 120;  // purposely defined in pixels and not in dp
	this.headerMenuViewHeight = "30dp";
	this.headerMenuViewsArray = [];
	this.ScrollableViewsArray = [];
	this.scrollableView = Ti.UI.createScrollableView({
  		showPagingControl:false
	});
	this.headerLeftDefaultDisplacement = "100dp";
	this.scrollableViewCount = 0;
	this.component = Ti.UI.createView({layout:"vertical"});
	
	
};

ScrollableViewWithHeaderMenu.prototype = new baseComponent("ScrollableViewWithHeaderMenu");
ScrollableViewWithHeaderMenu.prototype.constructor = ScrollableViewWithHeaderMenu;


ScrollableViewWithHeaderMenu.prototype.setData = function(_args){	
	var that = this;
	var headerMenuContainerMoving = null;
	if(_args.scrollableViews instanceof Array && ( _args.headerMenuTitles instanceof Array || _args.headerMenuViews instanceof Array)){
		var header = _args.headerMenuTitles || _args.headerMenuViews;
		if(header.length == _args.scrollableViews.length){
			//this.ScrollableViewsArray = _args.scrollableViews;
			this.scrollableView.setViews(_args.scrollableViews);
			this.headerMenuViewWidth = _args.headerMenuWidth || this.headerMenuViewWidth;
			this.headerMenuViewHeight = _args.headerMenuViewHeight || this.headerMenuViewHeight;
			this.scrollableViewCount = _args.scrollableViews.length;
			headerMenuContainerMoving = Titanium.UI.createView({
				top: 0,
   				width: (that.headerMenuViewWidth * that.scrollableViewCount)+"dp",
  	 			height: that.headerMenuViewHeight,
   				layout:"horizontal",
   				left:that.headerLeftDefaultDisplacement,
			});
		}
		else{
			throw "**ERROR** Number of header views and number of scrollable views mismatch ";
			return;
		}
	}
	else{
		throw "**ERROR** arguments must contain arrays of scrollable views and corresponding header menu  ";
		return;
	}
	if(_args.headerMenuTitles){
		for(var i =0; i<this.scrollableViewCount;i++ ){
			var headerMenuView = Ti.UI.createView({
				width :that.headerMenuViewWidth + "dp",
				height: that.headerMenuViewHeight + "dp",
			} );
			headerMenuView.add(
				Ti.UI.createLabel({
					text:_args.headerMenuTitles[i],
					font:{fontSize:16,fontFamily:"OpenSans-Light.ttf"},
					height:"20dp",
					color:_args.headerMenuColor || "#56585E",
				})
			);
			var buttonView = Ti.UI.createView({
				width :that.headerMenuViewWidth + "dp",
				height: that.headerMenuViewHeight + "dp",
				menuIndex: i.toString(),
			} );
			buttonView.addEventListener("click",function(e){
				that.scrollableView.scrollToView( parseInt(e.source.menuIndex));
			});
			headerMenuView.add(buttonView);
			this.headerMenuViewsArray.push(headerMenuView);
			headerMenuContainerMoving.add(headerMenuView);
		}
		
	}
	else if(_args.headerMenuViews){
		for(var i =0; i<this.scrollableViewCount;i++ ){
			_args.headerMenuViews[i].menuIndex = i.toString;
			_args.headerMenuViews[i].addEventListener("click",function(e){
				that.scrollableView.scrollToView( e.source.menuIndex);
			});
			headerMenuContainerMoving.add(_args.headerMenuViews[i]);
			this.headerMenuViewsArray.push(_args.headerMenuViews[i]);	
		}
	}
	
	var headerContainerNonMoving = Titanium.UI.createView({
   		top: 0,
   		width: Ti.UI.FILL,
   		height: that.headerMenuViewHeight,
  	});
  	headerContainerNonMoving.add(headerMenuContainerMoving);
  	this.component.add(headerContainerNonMoving);
  	this.scrollableView.addEventListener("scroll",function(e){
	//var decimal = Math.abs( Math.floor(e.currentPageAsFloat) - e.currentPageAsFloat);
	//var scalefactor = 1 - decimal * 0.4;
	//a .transform = matrix2d.scale(scalefactor);
	//e.view.animate(a);
	headerMenuContainerMoving.left = (that.headerLeftDefaultDisplacement - Math.floor(e.currentPageAsFloat * that.headerMenuViewWidth))+"dp";
});
  	this.component.add(that.scrollableView);
};

ScrollableViewWithHeaderMenu.prototype.resetData = function(_args){	

};

ScrollableViewWithHeaderMenu.prototype.reInit = function(_args){	
};

function calculateHeaderMenuTotalWidth(sizeOfHeaderMenuViewInDP,numberOfHeaderViews){
	if(typeof sizeOfHeaderMenuViewInDP == "string"  && (sizeOfHeaderMenuViewInDP.search("dp")>-1)){
		return (parseInt( sizeOfHeaderMenuViewInDP.slice(0,-2)) * numberOfHeaderViews ) + "dp";
	}
	
}



module.exports=ScrollableViewWithHeaderMenu;