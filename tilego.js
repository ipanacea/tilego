
Ti.API.info("initializing logger....");
var logging = {};
logging.getLogger = function(filename){
		var logger = require('/tilego/util/logger');
		return new logger(filename);
};
var $tl = function(){};
$tl.device = require('tilego/util/device');
$tl.moment = require('tilego/util/moment');
$tl.appAnalytics = require('tilego/util/Analytics');
$tl._ = require('tilego/util/lodash');
$tl.db = require('tilego/util/db');
$tl.xhr = require('tilego/util/network');
$tl.validate = require('tilego/util/Validate2');
$tl.oauth = require('tilego/util/oauth2');
$tl.style = require('tilego/windows/responsive_style');
$tl.ui = require('tilego/ui');

// attaching methods which handle destroy/stying etc to $tl
(function(){
	var fn = require('tilego/fn');
	$tl.getViewChildren = fn.getViewChildren;
	$tl.instanceOfTitaniumView = fn.instanceOfTitaniumView;
	$tl.destroy = fn.destroy;
	$tl.addStyle = fn.addStyle;
	$tl.removeStyle = fn.removeStyle;
	$tl.renderStyle = fn.renderStyle;
})();

// unnecessary steps to make the bloody titanium studio autocomplete using intellisense (they should call it lamesense)
$tl.getViewChildren = $tl.getViewChildren;
$tl.instanceOfTitaniumView = $tl.instanceOfTitaniumView;
/**
 * this is the actual method which destroys the views. Recursively traverses the view for children and starts
 * destroying them from leaf node onwards (first be removign from the view and then setting them to null)
 *
 * @method DestroyViews
 */

$tl.destroy = $tl.destroy;
/**
 * method to add tssStyle to a titanium view
 * @method addStyle
 * @param {View}
 * @param {String} tssStyle
 * */
$tl.addStyle = $tl.addStyle;
/**
 * method to remove tssStyle to a titanium view
 * @method removeStyle
 * @param {View}
 * @param {String} tssStyle
 * */
$tl.removeStyle = $tl.removeStyle;
$tl.renderStyle = $tl.renderStyle;

Object.freeze($tl);  
Ti.API.info("done loading tilego....");




