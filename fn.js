
var getViewChildren = function(view) {
  if(view){
  	return $tl._.union(view.children, view.views, view.section, view.data, view.sections);
	 
  }
};
exports.getViewChildren = getViewChildren;

var instanceOfTitaniumView  = function(view){
	if(view){
		try {
			var name = view.getApiName();
			if(name.indexOf("Ti.UI.") > -1){
				return true;
			}
		}
		catch(err) {
			logger.warn("passed in object is not an instance of titanium view- Ti.UI.View");
		}
	}
	return false;
};
exports.instanceOfTitaniumView = instanceOfTitaniumView;
/**
 * this is the actual method which destroys the views. Recursively traverses the view for children and starts
 * destroying them from leaf node onwards (first be removign from the view and then setting them to null)
 *
 * @method DestroyViews
*/
var destroy = function(view){
		function destroyView(view){
			var childViews = getViewChildren(view);
			if (childViews) {
				logger.debug("Views getting destroyed: "+view.getApiName()+" count:"+childViews.length);
        		for (var c = childViews.length - 1; c >= 0; c--) {
            		var tempView = childViews[c];
            		view.remove(childViews[c]);
            		this.destroy(tempView);
        		}
    		}
    		view = null;
    		return;
		}
		return destroyView(view);
};
exports.destroy = destroy;

/**
 * method to add tssStyle to a titanium view
 * @method addStyle
 * @param {View}
 * @param {String} tssStyle
 * */
var addStyle = function(view, styleString){
		var existingStyles = view[$tl.style.styleProperty] ? view[$tl.style.styleProperty].split($tl.style.delimiter) : [];
		var stylesToAdd = styleString.split($tl.style.delimiter);
		var finalStyles = $tl._.union(existingStyles,stylesToAdd);
		
		var finalStyleProperties = $tl.style.getStyle(finalStyles);
		processStyle(view,finalStyleProperties);
		view[$tl.style.styleProperty] = finalStyles.join($tl.style.delimiter);
	
};
exports.addStyle = addStyle;

/**
 * method to remove tssStyle to a titanium view
 * @method removeStyle
 * @param {View}
 * @param {String} tssStyle
 * */
var removeStyle = function(view, styleString){
	// check if a style is already associated with the view
	if(view[$tl.style.styleProperty]){
		var existingStyles = view[$tl.style.styleProperty].split($tl.style.delimiter);
		var stylesToRemove = styleString.split($tl.style.delimiter);
		
		var stylesPropertiesToRemove = replaceSameKeys($tl.style.getStyle(stylesToRemove),$tl.style.RESET);
		processStyle(view, stylesPropertiesToRemove);
		var finalStyles = $tl._.difference(existingStyles,stylesToRemove);
		var finalStyleProperties = $tl.style.getStyle(finalStyles);
		processStyle(view, finalStyleProperties);
		view[$tl.style.styleProperty] = finalStyles.join($tl.style.delimiter);
	}
};
exports.removeStyle = removeStyle;

/**
 * This is the internal private method which applies styles to views/window.
 * It recursively traverses to the leafnode in the view/window, checks if there style element\
 * associated with it, if present applies the style in the following precedence.
 * style explicitly defined in the view/window >> in-order merge of tss selectors specified in the style variable 'tssStyle'
 * @private
 * @method renderStyle
 */
var renderStyle = function(view, renderChildren){
	var isDirty = view.isDirty || true;
	var isChildrenDirty = view.isChildrenDirty || true;
	var childViews = getViewChildren(view);
	if (childViews && isChildrenDirty && renderChildren) {
		for (var c = childViews.length - 1; c >= 0; c--) {
            var tempView = childViews[c];
            renderStyle(tempView,renderChildren);
        }
    }
    var tssStyle = view[$tl.style.styleProperty];
    if(tssStyle && isDirty){
    	if( $tl._.isString(tssStyle)){
    			processStyle(view, $tl._.extend( $tl.style.getStyle(tssStyle.split($tl.style.delimiter)),  getViewProperties(view))  );	
    	}
    }
    view.isDirty = false;
    view.isChildrenDirty = false;
};
exports.renderStyle = renderStyle;


//private functions

function processStyle(view, properties, defaults){
	var tssStyle = view[$tl.style.styleProperty];
	var finalStyleProperties = defaults ? $tl._.defaults(properties,defaults) : properties;
	view.applyProperties(finalStyleProperties);
	var name = view.getApiName() + "_"+view.name;
	logger.debug("Applying style on:" + name + " tssStyle: "+tssStyle + " properties: "+ JSON.stringify(finalStyleProperties) );
}


function replaceSameKeys(target, dictionary){
	for (var key in target) {
		if (target.hasOwnProperty(key)) {
    		if(dictionary.hasOwnProperty(key)){
    			target[key] = dictionary[key];
    		}
 	 	}
	}
	return target;
}

function getViewProperties(view){
	return $tl._.clone(view);
}

