var logger = logging.getLogger("tilego/ui.js");

var UIFactory=(function(){
	
	
	var windowPool = {};
	
	var componentFileDictionary = {
		BaseComponent : "tilego/components/base_component",
		Table : "tilego/components/table",
		ScrollableViewWithHeaderMenu : "tilego/components/scrollable_view_with_header_menu",
		Calendar : "tilego/components/calendar",
		Grid:"tilego/components/grid",
		CalendarEventList:"tilego/components/calendar_event_list",
		ArticleTile: "tilego/components/article_tile",
		FormComponent: "tilego/components/form_component",
		DateWidget : "tilego/components/date_widget",
	};
	
	var windowFileDictionary = {
		HomeScreen : "ui/common/home_screen",
		Search : "ui/common/search",
		Profile : "ui/common/profile",
		DrugDetail : "ui/common/drug_detail",
		LoginSignup : "ui/common/login_signup",
		MyDay : "ui/common/myday",
		CalendarEvents:"ui/common/calendar_events",
		CalendarEventDetail:"ui/common/calendar_event_detail",
		HTMLBrowser:"ui/common/html_browser",
		ArticleTileView : "ui/common/article_tile_view",
		EULA: "ui/common/eula",
		HomeView : "ui/common/home_view",
		TabletHome : "ui/common/tablet_home",
	};
	function generateNameDictionary(sourceDictionary){
		var nameDictionary = {};
		for (var key in sourceDictionary) {
  			if (sourceDictionary.hasOwnProperty(key)) {
    			nameDictionary[key] = key;
  			}
		}
		return nameDictionary;
	};
	
	var component = generateNameDictionary(componentFileDictionary);
	var window = generateNameDictionary(windowFileDictionary);
	
	var getComponent = function(_args){
		if(_args){
			var componentFilePath = componentFileDictionary[_args.componentType];
			if(componentFilePath){
				logger.info("**UIFactory** creating component: "+_args.componentType);
				var component = require(componentFilePath);
				var componentInstance = new component(options.data);
				return componentInstance;
			}
		}
	};
	var getWindow = function(_args){
		if(_args){
			var windowFilePath = windowFileDictionary[_args.windowType];
			if(windowFilePath){
				if(windowPool.hasOwnProperty(_args.windowType)){
					logger.info("**UIFactory** window: "+_args.windowType + " already created...returning from pool");
					return windowPool[_args.windowType];
				}else{
					logger.info("**UIFactory** creating window: "+_args.windowType);
					var window = require(windowFilePath);
					var windowInstance = new window(options.data);
					windowPool[_args.windowType] = windowInstance;
					return windowInstance;
				}
			}
			
		}
	};
	
	var getWindowFilePath = function(name){
		return windowFileDictionary[name];
	};
	
	var getComponentFilePath = function(name){
		return componentFileDictionary[name];
	};
	
	return {
	getComponent : getComponent,
	getWindow : getWindow,
	component : component,
	window : window,
	getWindowFilePath : getWindowFilePath,
	getComponentFilePath : getComponentFilePath,	
	};
})();

exports.getComponent = UIFactory.getComponent;
exports.getWindow = UIFactory.getWindow;
exports.window = UIFactory.window;
exports.component = UIFactory.component;
exports.getComponentFilePath = UIFactory.getComponentFilePath;
exports.getWindowFilePath = UIFactory.getWindowFilePath;
