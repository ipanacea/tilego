function Util() {
}

Util.Db = require('tilego/util/db');
Util.XHR = require('tilego/util/network');
Util.Device = require('tilego/util/device');
Util.Moment = require('tilego/util/moment');
Util.Validate = require('tilego/util/Validate2');
Util._ = require('tilego/util/lodash');
Util.Analytics = require('tilego/util/Analytics');
Util.functions = {};

Util.functions.getDateObj = function(gmtDateString) {
	var momentObj = null;
	var dateObj = {};
	if (gmtDateString) {
		// Assumption: gmtDateString is of the format - '2014-06-22T21:00:00' GMT.
		// Add one of these: '+00:00 +0000 -00:00 -0000' at the end to specify zone explicitly.
		momentObj = Util.Moment(gmtDateString, 'YYYY-MM-DDTHH:mm:ss Z');
	} else {
		momentObj = Util.Moment();
	}

	dateObj.dayName = momentObj.format("dddd");
	dateObj.date = momentObj.format("D");
	dateObj.monthName = momentObj.format("MMM");
	dateObj.getToday = momentObj.format("YYYY-MM-D");
	dateObj.getTimeInAMPM = momentObj.format("h:mm A");
	dateObj.hours = momentObj.format("H");
	dateObj.minutes = momentObj.format("mm");
	dateObj.toDateString = momentObj.format("ddd MMM D YYYY");
	dateObj.toString = momentObj.format();
	
	dateObj.nextNthDay = function(numOfDays) {
		var temp = momentObj.clone();
		temp.add('d', numOfDays);
		return temp;
	};

	dateObj.getCurrentWeek = function() {
		var week = {};
		// TODO: Preserve date with timestamp so that we can send date + timestamp when using backend api.
		
		week.start = momentObj.format("YYYY-MM-D");
		/*
		 * We take first day of next week to be the week.end here which is actually 1 sec more than last day of this week.
		 * First day of next week is more relavant (an easy to use in queries) than last day of current week as we dont refer these dates with timestamps in queries.
		 * 2014-01-22 => 2014-01-22 00:00:00 => 2014-01-21 23:59:59 + 00:00:01
		 */
		week.end = dateObj.nextNthDay(7).format("YYYY-MM-D");

		return week;
	};

	return dateObj;
};
Util.functions.getElapsedDurationInHours = function(gmtDateString) {
	var eventTime = Util.functions.getDateObj(gmtDateString);

	var elapsedDuration = Util.Moment() - eventTime.object;
	if (elapsedDuration > 0) {
		return Util.Moment.duration(elapsedDuration).humanize() + " ago";
	} else {
		return "in " + Util.Moment.duration(elapsedDuration).humanize();
	}
};


Util.functions.getDaysArray = function(year, month) {
    var numDaysInMonth, daysInWeek, daysIndex, index, i, l, daysArray;

    numDaysInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
    daysInWeek = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    daysIndex = { 'Sun': 0, 'Mon': 1, 'Tue': 2, 'Wed': 3, 'Thu': 4, 'Fri': 5, 'Sat': 6 };
    index = daysIndex[(new Date(year, month - 1, 1)).toString().split(' ')[0]];
    daysArray = [];
    var monthLength = numDaysInMonth[month - 1];
    if (this.month == 1) { // February only!
  		if ((this.year % 4 == 0 && this.year % 100 != 0) || this.year % 400 == 0){
    		monthLength = 29;
  		}
	}

    for (i = 0, l = monthLength; i < l; i++) {
        daysArray.push([(i + 1),daysInWeek[index++]]);
        if (index == 7) index = 0;
    }

    return daysArray;
};
Util.functions.getDaysObject = function(year, month) {
    var numDaysInMonth, daysInWeek, daysIndex, index, i, l, daysArray;

    numDaysInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
    daysInWeek = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    daysIndex = { 'Sun': 0, 'Mon': 1, 'Tue': 2, 'Wed': 3, 'Thu': 4, 'Fri': 5, 'Sat': 6 };
    index = daysIndex[(new Date(year, month - 1, 1)).toString().split(' ')[0]];
    daysArray = [];
    var monthLength = numDaysInMonth[month - 1];
    if (this.month == 1) { // February only!
  		if ((this.year % 4 == 0 && this.year % 100 != 0) || this.year % 400 == 0){
    		monthLength = 29;
  		}
	}

    for (i = 0, l = monthLength; i < l; i++) {
        daysArray.push([(i + 1),daysInWeek[index++]]);
        if (index == 7) index = 0;
    }

    return daysArray;
};

Util.functions.mergeObjs = function(obj1, obj2){
    var obj3 = {};
    for (var attrname in obj1) { obj3[attrname] = obj1[attrname]; }
    for (var attrname in obj2) { obj3[attrname] = obj2[attrname]; }
    return obj3;

};
Util.functions.call_base = function(object, method, args) {
    // We get base object, first time it will be passed object,
    // but in case of multiple inheritance, it will be instance of parent objects.
    var base = object.hasOwnProperty('_call_base_reference') ? object._call_base_reference : object,
    // We get matching method, from current object,
    // this is a reference to define super method.
            object_current_method = base[method],
    // Temp object wo receive method definition.
            descriptor = null,
    // We define super function after founding current position.
            is_super = false,
    // Contain output data.
            output = null;
    while (base !== undefined) {
        // Get method info
        descriptor = Object.getOwnPropertyDescriptor(base, method);
        if (descriptor !== undefined) {
            // We search for current object method to define inherited part of chain.
            if (descriptor.value === object_current_method) {
                // Further loops will be considered as inherited function.
                is_super = true;
            }
            // We already have found current object method.
            else if (is_super === true) {
                // We need to pass original object to apply() as first argument,
                // this allow to keep original instance definition along all method
                // inheritance. But we also need to save reference to "base" who
                // contain parent class, it will be used into this function startup
                // to begin at the right chain position.
                object._call_base_reference = base;
                // Apply super method.
                output = descriptor.value.apply(object, args);
                // Property have been used into super function if another
                // call_base() is launched. Reference is not useful anymore.
                delete object._call_base_reference;
                // Job is done.
                return output;
            }
        }
        // Iterate to the next parent inherited.
        base = Object.getPrototypeOf(base);
    }
};

module.exports = Util;