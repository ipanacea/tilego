/**
* Analytics class for tracking end user metrics such as important events, perforamnce metrics etc
* @class Analytics
* @constructor
* @module tilego
* @submodule Util
*/
var Analytics = (function(){
	//constants
	var kElapsedTimeLabel = "el";
	var kStartTimeLabel = "st";
	var kEndTimeLabel = "et";
	var kTransactionNameLabel = "tn";
	var kMeasureLabel = "measures";
	var kOptionalAttributesLabel = "oAttr";
	var kMeasureNameLabel = "mn";


	var performanceTrackingDictionary = {};
	
	var currentActiveTransaction;
	
	function getElapsed(start,end){
		return end - start;
	}
	/**
 	* Method to start a transaction
 	*
 	* @method startTransaction
 	* @param {String} name of the transaction (required)
 	*/
	var startTransaction = function(name){
		if(name){
			if(!performanceTrackingDictionary.hasOwnProperty(name)){
				currentActiveTransaction = name;
				performanceTrackingDictionary[name] = {};
				performanceTrackingDictionary[name][kTransactionNameLabel] = name;
				performanceTrackingDictionary[name][kStartTimeLabel] = new Date().getTime();
				performanceTrackingDictionary[name][kMeasureLabel] = {};
			}
		}
	};
	/**
 	* Method to end a transaction
 	*
 	* @method endTransaction
 	* @param {String} name of the transaction (required)
 	*/
	var endTransaction = function(name){
		if(name){
			if(performanceTrackingDictionary.hasOwnProperty(name)){
				performanceTrackingDictionary[name][kEndTimeLabel] = new Date().getTime();
				performanceTrackingDictionary[name][kElapsedTimeLabel] = getElapsed(performanceTrackingDictionary[name][kStartTimeLabel],performanceTrackingDictionary[name][kEndTimeLabel]);
				currentActiveTransaction = null;
			}
		}
	};
	
	/**
 	* Method to mark the start of an event
 	*
 	* @method mark
 	* @param {String} name to the tag the measurement (required)
 	* @param {String} name of the transaction to which the measurement belongs to.  (optional) if nothing is passed, then the measurement
 	* will be added to the currently active transaction 
 	*/
	var mark = function(name,transactionName){
		if(name){
			var tn = transactionName || currentActiveTransaction;
			if(tn){
				if(performanceTrackingDictionary.hasOwnProperty(tn)){
					performanceTrackingDictionary[tn][kMeasureLabel][name] = {};
					performanceTrackingDictionary[tn][kMeasureLabel][name][kStartTimeLabel] = new Date().getTime();
				}
			}
		}
	};
	
	/**
 	* Method to end time measurement for the given mark
 	*
 	* @method endMark
 	* @param {String} name to the tag the measurement (required)
 	* @param {String} name of the transaction to which the measurement belongs to.  (optional) if nothing is passed, then the measurement
 	* will be added to the currently active transaction 
 	*/
	var endMark = function(name,transactionName){
		if(name){
			var tn = transactionName || currentActiveTransaction;
			if(tn){
				if(performanceTrackingDictionary.hasOwnProperty(tn)){
					if(performanceTrackingDictionary[tn][kMeasureLabel].hasOwnProperty(name)){
						performanceTrackingDictionary[tn][kMeasureLabel][name][kEndTimeLabel] = new Date().getTime();
						performanceTrackingDictionary[tn][kMeasureLabel][name][kElapsedTimeLabel] = getElapsed(performanceTrackingDictionary[tn][kMeasureLabel][name][kStartTimeLabel],performanceTrackingDictionary[tn][kMeasureLabel][name][kEndTimeLabel]);
					}
					
				}
			}
		}
	};
	
	/**
 	* clears the internal dictionary which holds the measurements 
 	*
 	* @method clear
 	* @param {String} transactionName (optional) . 
 	* if provided, it clears the measurements associaited with the transaction name, otherwise clears the whole dictionary
 	*/
	var clear = function(transactionName){
		if(transactionName){
			delete performanceTrackingDictionary[transactionName];
		}
		else
			performanceTrackingDictionary = {};
	};
	
	/**
 	* prints json formatted string containing measurments
 	*
 	* @method print
 	* @param {String} transactionName (optional) . 
 	* if provided, it prints the measurements associaited with the transaction name, otherwise prints the whole dictionary
 	*/
	var print = function(transactionName){
		Ti.API.debug(toJson(transactionName));
	};
	
	/**
 	* returns json formatted string containing measurments
 	*
 	* @method toJson
 	* @return {String} json formatted string 
 	* @param {String} transactionName (optional) .
 	* if provided, it returns the measurements associaited with the transaction name, otherwise returns the whole dictionary in json string
 	*/
	var toJson = function(transactionName){
		if(transactionName){
			if(performanceTrackingDictionary.hasOwnProperty(transactionName)){
				return JSON.stringify(performanceTrackingDictionary[transactionName]);
			}
		}else
			return JSON.stringify(performanceTrackingDictionary);
	};
	
	//publicly exposed methods
	return {
		print : print,
		toJson : toJson,
		clear : clear,
		mark : mark,
		endMark : endMark,
		startTransaction : startTransaction,
		endTransaction : endTransaction,
	};
	
})();

exports.print = Analytics.print;
exports.toJson = Analytics.toJson;
exports.clear = Analytics.clear;
exports.mark = Analytics.mark;
exports.endMark = Analytics.endMark;
exports.startTransaction = Analytics.startTransaction;
exports.endTransaction = Analytics.endTransaction;
