var logger = logging.getLogger("device.js");

exports.properties = {};

exports.properties.osname = Ti.Platform.osname;  //ios android
exports.properties.version = Ti.Platform.version; //os version
exports.properties.displayHeight = Ti.Platform.displayCaps.platformHeight;
exports.properties.displayWidth = Ti.Platform.displayCaps.platformWidth;
exports.properties.displayDensity = Ti.Platform.displayDensity;
exports.properties.displayDpi = Ti.Platform.displayDpi;
exports.properties.platformModel = Ti.Platform.model;  // 'google_sdk'  'Simulator'
logger.info("model: "+exports.properties.platformModel);    
if(exports.properties.osname === 'android'){
  exports.properties.displayXdpi = Ti.Platform.displayCaps.xdpi;
  exports.properties.displayYdpi = Ti.Platform.displayCaps.ydpi;
  exports.properties.displayLogicalDensityFactor = Ti.Platform.displayCaps.logicalDensityFactor;
}
//private functions
function checkTablet() {
    var platform = exports.properties.osname;

    switch (platform) {
      case 'ipad':
        return true;
      case 'android':
        var psc = Ti.Platform.Android.physicalSizeCategory;
        var tiAndroid = Ti.Platform.Android;
        return psc === tiAndroid.PHYSICAL_SIZE_CATEGORY_LARGE || psc === tiAndroid.PHYSICAL_SIZE_CATEGORY_XLARGE;
      default:
        return Math.min(
          Ti.Platform.displayCaps.platformHeight,
          Ti.Platform.displayCaps.platformWidth
        ) >= 400;
    }
  }

var isTablet = checkTablet();

exports.isTablet = function(){
	return isTablet;
};

exports.isIOS = function(){
	if (exports.properties.osname === "ipad" || exports.properties.osname === "iphone" )
		return true;
	else
		return false;
};

exports.isAndroid = function(){
	if (exports.properties.osname === "android")
		return true;
	else
		return false;
};


function printDeviceProperties(){
	for (var key in exports.properties) {
		if (exports.properties.hasOwnProperty(key)) {
    	logger.debug("property: "+key + " value: "+exports.properties[key]);
    	}
	}
}
printDeviceProperties();
