
OAUTH2 = function(){};

var OAUTHCLIENTID = "oauthClientId";
var OAUTHCLIENTSECRET = "oauthClientSecret";
var OAUTHREDIRECTURL = "oauthRedirectUri";
var OAUTHAUTHSERVER = "oauthAuthServer";
var OAUTHGRANTTYPE = "grant_type";
var OAUTHUSERNAME = "oauthUsername";
var OAUTHPASSWORD = "oauthPassword";

var OAUTHTOKEN = "access_token";
var OAUTHTOKENTYPE = "token_type";
var OAUTHTOKENEXPIRY = "expires_in";
var OAUTHREFRESHTOKEN = "refresh_token";
var OAUTHSCOPE = "scope";


var TIME_RESERVE = 120;

OAUTH2.init = function(){
	var clientId = "spinach";
	var clientSecret = "123456";
	var redirectUri  = "";
	var	authServer   = "http://127.0.0.1:8000/o/token/";
	var grantType = "password";
	Ti.App.Properties.setString(OAUTHCLIENTID, clientId);
	Ti.App.Properties.setString(OAUTHCLIENTSECRET, clientSecret);
	Ti.App.Properties.setString(OAUTHREDIRECTURL, redirectUri);
	Ti.App.Properties.setString(OAUTHAUTHSERVER, authServer);
};
OAUTH2.getClientDetails = function(){
	var hash = {};
	hash[OAUTHCLIENTID] = Ti.App.Properties.getString(OAUTHCLIENTID);
	hash[OAUTHCLIENTSECRET] = Ti.App.Properties.getString(OAUTHCLIENTSECRET);
	hash[OAUTHREDIRECTURL] = Ti.App.Properties.getString(OAUTHREDIRECTURL);
	hash[OAUTHAUTHSERVER] = Ti.App.Properties.getString(OAUTHAUTHSERVER);
	return hash;
};


OAUTH2.setUsernamePassword = function(username,password){
	Ti.App.Properties.setString(OAUTHUSERNAME, username);
	Ti.App.Properties.setString(OAUTHPASSWORD, password);
};

OAUTH2.getUsernamePassword = function(username,password){
	var hash = {};
	hash[OAUTHUSERNAME] = Ti.App.Properties.getString(OAUTHUSERNAME);
	hash[OAUTHPASSWORD] = Ti.App.Properties.getString(OAUTHPASSWORD);
	return hash;
};


OAUTH2.getAccessToken  = function(){
	return Ti.App.Properties.getString(OAUTHTOKEN);
};

OAUTH2.authorize = function(username,password){
	
	Ti.API.info("OAUTH2.authorize()");

	if(!this.getAccessToken()){
		Ti.API.info("No access token saved.");
		getTokenFromServer();
	}
	else if(isAccessTokenExpired()){
		Ti.API.info("Access token expired.");
		refreshAccessToken();
	}
	else {
		Ti.API.info("Access token expires in = " + accessTokenExpireIn());
		return getRequestHeader();
	}
	
};

// private functions
var getOauthAuthServerURI = function(){
	return Ti.App.Properties.getString(OAUTHAUTHSERVER);
};

var getPostParams = function(){
	var hash = {};
	hash["client_id"] = Ti.App.Properties.getString(OAUTHCLIENTID);
	hash["client_secret"] = Ti.App.Properties.getString(OAUTHCLIENTSECRET);
	hash["grant_type"] = "password";
	hash["username"] = Ti.App.Properties.getString(OAUTHUSERNAME);
	hash["password"] = Ti.App.Properties.getString(OAUTHPASSWORD);
	Ti.API.info('OAUTH2 post params ' + JSON.stringify(hash));
	return hash;
};
var getRefreshTokenPostParams = function() {
	var hash = {};
	hash["client_id"] = Ti.App.Properties.getString(OAUTHCLIENTID);
	hash["client_secret"] = Ti.App.Properties.getString(OAUTHCLIENTSECRET);
	hash["grant_type"] = "refresh_token";
	hash["refresh_token"] = Ti.App.Properties.getString(OAUTHUSERNAME);
	Ti.API.info('OAUTH2 refresh token post params ' + JSON.stringify(hash));
	return hash;
};

var getTokenFromServer = function(){
	var url = getOauthAuthServerURI();
	var data = getPostParams();
	var onError = function(){};
	$tl.xhr.post(url,data,oauthCallback,onError);
};
var oauthCallback = function(result) {
	var data;
	try {
			data = JSON.parse(result.data);
			
	} catch(error) {
		Ti.API.info("JSON Parse Error! statusText: " + result.statusText);
		return;
	}
	if(data){
		Ti.API.info('OAUTH2 callback: ' + result.data);
		Ti.App.Properties.setString(OAUTHTOKEN, data.access_token);
		Ti.App.Properties.setString(OAUTHREFRESHTOKEN, data.refresh_token);
		Ti.App.Properties.setDouble(OAUTHTOKENEXPIRY, toSeconds(new Date().valueOf()) + data.expires_in - TIME_RESERVE);
	}
};

var toSeconds = function(ms) {
	return parseInt((ms / 1000).toFixed());
};
var isAccessTokenExpired = function(){
	var res = false;
	if(accessTokenExpireIn() <= 0) {
			res = true;
	}
	Ti.API.info('OAUTH2.isAccessTokenExpired(): ' + res);
	return res;
};

var accessTokenExpireIn = function() {
	var res = Ti.App.Properties.getDouble('expires_in') - toSeconds(new Date().valueOf());
	Ti.API.info('AuthModule.accessTokenExpireIn() return ' + res);
	return res;
};
var refreshAccessToken = function() {
	var url = getOauthAuthServerURI();
	var data = getRefreshTokenPostParams();
	var onError = function(){};
	$tl.xhr.post(url,data,oauthCallback,onError);
};
var getRequestHeader = function() {
	Ti.API.info('OAUTH2.getRequestHeader()');
			return {
				requestHeader: {
					value: 'Bearer ' + Ti.App.Properties.getString('access_token'),
					name:  'Authorization'
				}
			};
		};
OAUTH2.init();
exports.getClientDetails = OAUTH2.getClientDetails;
exports.setUsernamePassword = OAUTH2.setUsernamePassword;
exports.getUsernamePassword = OAUTH2.getUsernamePassword;
exports.getAccessToken =  OAUTH2.getAccessToken;
exports.authorize = OAUTH2.authorize;