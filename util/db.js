/***************************************************
# DB:
DB is a wrapper around Titanium's Datavase. 
## Usage:
In your app.js (or elsewhere), call:

```javascript
var db= require("/db");

db.select("leagueTable", ["id","name"]);
db.select("leagueTable", "*");
```


####################### END of EXAMPLE ###############################





***************************************************/
var DBNAME="Spinach";






exports.select = function(tableName,selectColumns) {
	var db = Titanium.Database.open(DBNAME);
	try{
		var rows = db.execute("SELECT " + selectColumns + " FROM " + tableName);
		var rowsToJson = [];
		Titanium.API.info('select statement returned #rows = ' + rows.getRowCount());
		while (rows.isValidRow()) {
			// TODO: check if a better iterator exists
			var tableRow = {};
			for ( i = 0; i < rows.getFieldCount(); i++) {
				tableRow[rows.fieldName(i)] = rows.field(i);
			}
			rowsToJson.push(tableRow);
			rows.next();
		}
		rows.close();
		db.close();
		return rowsToJson;

	}
	catch(err){
		db.close();
		Titanium.API.error("Error executing select statement on table : "+tableName+"with columns : "+selectColumns);
	}
	
		
	
};

exports.selectWithWhere = function(tableName,selectColumns,whereClause) {
	var db = Titanium.Database.open(DBNAME);
	try{
		var rows = db.execute("SELECT " + selectColumns + " FROM " + tableName+" "+whereClause);
		var rowsToJson = [];
		Titanium.API.info('select statement returned #rows = ' + rows.getRowCount());
		while (rows.isValidRow()) {
			// TODO: check if a better iterator exists
			var tableRow = {};
			for ( i = 0; i < rows.getFieldCount(); i++) {
				tableRow[rows.fieldName(i)] = rows.field(i);
			}
			rowsToJson.push(leagueRow);
			rows.next();
		}
		rows.close();
		db.close();
		return rowsToJson;

	}
	catch(err){
		db.close();
		Titanium.API.error("Error executing select statement on table : "+tableName+" with columns : "+selectColumns+" clause: "+whereClause);
	}
	
		
	
};

exports.clearTable = function(tableName) {
	var db = Titanium.Database.open(DBNAME);
	try{
		var rows = db.execute("DELETE FROM " +tableName);
		Titanium.API.info('select statement returned #rows = ' + rows.getRowCount());
		db.close();
		return rows.getRowCount();

	}
	catch(err){
		db.close();
		Titanium.API.error("Error clearing table : "+tableName);
	}
	
		
	
};


exports.multiInsert = function(tableName,cols, valuesArray) {
	var db = Titanium.Database.open(DBNAME);
	try{
		db.execute('BEGIN'); // begin the transaction
		for(columnValues in valuesArray){
			var queryString= "INSERT INTO "+tableName+"("+cols+") VALUES("+columnValues+")";
			Titanium.API.info(queryString);
			db.execute( queryString );
		}
		db.execute('COMMIT');
		db.close();
	}
	catch(err){
		db.close();
		Titanium.API.error("Error executing multiInsert statement on table : "+tableName);
	}
};

exports.insert = function(tableName,columns,values) {
	var db = Titanium.Database.open(DBNAME);
	try{
		var queryString= "INSERT INTO "+tableName+"("+columns+") VALUES("+values+")";
		Titanium.API.info(queryString);
		db.execute( queryString );
		db.close();
	}
	catch(err){
		db.close();
		Titanium.API.error("Error executing multiInsert statement on table : "+tableName);
	}
};

exports.execute = function(queryString) {
	var db = Titanium.Database.open(DBNAME);
	try{
		Titanium.API.info(queryString);
		db.execute( queryString );
		db.close();
	}
	catch(err){
		db.close();
		Titanium.API.error("Error executing : "+queryString);
	}
};


exports.update = function(tableName,colValuesPairArray,whereCondition) {
	var db = Titanium.Database.open(DBNAME);
	try{
		var queryString= "UPDATE "+tableName+"SET "+colValuesPairArray+"WHERE "+whereCondition;
		Titanium.API.info(queryString);
		db.execute( queryString );
		db.close();
		return; 
	}
	catch(err){
		db.close();
		Titanium.API.error("Error executing update statement on table : "+tableName);
	}
};

/***********************************  app specific public functions     ***************/


// =================

//private functions 
