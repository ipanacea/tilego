/**
 * 
 # Rate
CommonJS module popping the 'rate-my-app' question at just the right time.

## Why?
We all know these popups asking us to Please rate my app. Getting lots of ratings is important for high rankings in both Apple and Android app stores. But popping the question at the wrong time could get you low rating and bad reviews.

This module deals with that.

## Oneliner
At minimum it takes just a single line of code to use. By default it brings up the question after 3 days when the code has been executed at least 3 times. It then gets your app’s Bundle ID from Ti.App.id and looks up the required Apple ID online.

```
require('rate').plus();
```

## Timing
I personally like to collect points at places where the user has positive interaction with the app, but wait before popping the question until he has finished doing it. You can achieve this in the following way:

```
var rate = require('rate');
 
// Put this on a happy place to add 1 (or more) points,
// but NOT ask the question if the required points have been met:
rate.plus(1, false);
 
// Put this on a place fit for asking the question if points have been met,
// without adding any points itself:
rate.test();
```

## Options
You can further tweak the behavior using these options:

```
var rate = require('rate');
 
// Set texts
rate.title = 'Pleeeeease rate!';
rate.message = 'I would be so thankful!';
rate.yes = 'Fine';
rate.later = 'Maybe later';
rate.never = 'Forget it';
 
// Set triggers
rate.pointsBetween = 100; // Points before asking and between each retry
rate.daysBetween = 10; // Days before asking and between each retry
rate.eachVersion = true; // Ask again every version (unless user chose 'never')
 
// Set Apple ID (found in iTunes Connect) manually
rate.appleId = 123456;
 
// Reset all triggers (including if user chose 'never', so be aware!)
rate.reset();
 
// Add 1 point and test if we should ask (returns TRUE if question was aked)
rate.plus();
 
// Add more points and do not test
rate.plus(5, false);
 
// Just test (returns TRUE if question was aked)
rate.test();
 
// Just ask
rate.open();
```

## Callback
You can set a simple callback to get notified of the response of the user, so you can attach some analytics. For example:

```
rate.listener = function(e) {
    switch (e.type) {
        case 'yes': break;
        case 'never': break;
        case 'later': break;
    }
}; 
 */


var _properties = {}, _platform;

function plus(_points, _show) {

    if (typeof _points !== 'number') {
        _points = 1;
    }

    return _trigger(_points, _show);
}

function test() {
    return _trigger();
}

function ask() {

    if (typeof _platform !== 'string') {
        _platform = Ti.Platform.name;
    }

    if (_platform === 'iPhone OS' && !exports.appleId) {

        _lookup(function(result) {

            if (!result) {
                Ti.API.debug('[RATE] Lookup failed.');
                return;
            }

            exports.appleId = result.trackId;

            return ask();
        });

        return;
    }

    var buttonNames = [exports.yes, exports.later, exports.never];
    var cancel = buttonNames.length - 1;

    var alertDialog = Titanium.UI.createAlertDialog({
        title: exports.title,
        message: exports.message,
        buttonNames: buttonNames,
        cancel: cancel
    });

    alertDialog.addEventListener('click', function(e) {

        if (buttonNames[e.index] === exports.yes) {
            Ti.App.Properties.setString('rate_done', Ti.App.version);

            open();

            if (exports.listener !== null) {
                exports.listener({
                    type: 'yes'
                });
            }

        } else if (buttonNames[e.index] === exports.never) {
            Ti.App.Properties.setBool('rate_never', true);

            if (exports.listener !== null) {
                exports.listener({
                    type: 'never'
                });
            }

        } else {

            if (exports.listener !== null) {
                exports.listener({
                    type: 'later'
                });
            }
        }

        return;
    });

    alertDialog.show();

    return;
}

function open() {

    if (Ti.Platform.name === 'android') {
        Ti.Platform.openURL('market://details?id=' + Ti.App.id);

    } else if (Ti.Platform.name === 'iPhone OS') {

        if (!exports.appleId) {
            _lookup(function(result) {

                if (!result) {
                    Ti.API.debug('[RATE] Lookup failed.');
                    return;
                }

                exports.appleId = result.trackId;
                return open();
            });

            return;
        }

        Ti.Platform.openURL('http://itunes.apple.com/app/id' + exports.appleId);
    }

    return;
}

function reset() {
    Ti.API.debug('[RATE] Reset.');

    Ti.App.Properties.removeProperty('rate_done');
    Ti.App.Properties.removeProperty('rate_never');
    Ti.App.Properties.removeProperty('rate_points');
    Ti.App.Properties.removeProperty('rate_asked');

    return;
}

function _trigger(_points, _show) {

    if (Ti.App.Properties.getBool('rate_never', false) === true) {
        Ti.API.debug('[RATE] Rating disabled by user.');
        return false;
    }

    var rate_done = Ti.App.Properties.getString('rate_done');

    if (exports.eachVersion ? (rate_done === Ti.App.version) : rate_done) {
        Ti.API.debug('[RATE] Rating already done.');
        return false;
    }

    var points = Ti.App.Properties.getInt('rate_points', 0);

    if (_points) {
        points = points + (_points || 1);
        Ti.API.debug('[RATE] Rating points changed to: ' + points);
    }

    var now = (new Date() / 1000),
        checked = Ti.App.Properties.getInt('rate_asked', 0);

    if (_show !== false) {

        if (checked === 0) {
            Ti.App.Properties.setInt('rate_asked', now);
            checked = now;
        }

        if (points < exports.pointsBetween) {
            Ti.API.debug('[RATE] Not enough points: ' + points + ' of ' + exports.pointsBetween);
            _show = false;

        } else if ((now - checked) < (exports.daysBetween * 86400)) {
            Ti.API.debug('[RATE] Not enough days' + (checked ? (': ' + Math.round((now - checked) / 86400) + ' of ' + exports.daysBetween) : ''));
            _show = false;
        }
    }

    if (_show !== false) {
        Ti.API.debug('[RATE] Rating triggered!');

        Ti.App.Properties.setInt('rate_points', 0);
        Ti.App.Properties.setInt('rate_asked', now);

        ask();

        return true;

    } else {
        Ti.App.Properties.setInt('rate_points', points);
    }

    return false;
}

function _lookup(_callback) {
    var xhr = Ti.Network.createHTTPClient({
        onload: function(e) {
            if (xhr.status === 200 && this.responseText) {
                try {
                    var json = JSON.parse(this.responseText);

                    if (json.resultCount === 1) {
                        _callback(json.results[0]);
                        return;

                    } else {
                        Ti.API.error('[RATE] LOOKUP ERROR ' + this.responseText);
                    }

                } catch (err) {
                    Ti.API.error('[RATE] LOOKUP ERROR ' + JSON.stringify(err));
                }
            }

            _callback();
            return;
        },
        onerror: function(e) {
            Ti.API.error('[RATE] LOOKUP ERROR ' + JSON.stringify(e.error));
            _callback();
            return;
        }
    });

    var url = 'http://itunes.apple.com/lookup?';

    if (exports.appleId) {
        url = url + 'id=' + exports.appleId;
    } else {
        url = url + 'bundleId=' + Ti.App.id;
    }

    xhr.open('GET', url);
    xhr.send();

    return;
}

exports.title = 'Like our app?';
exports.message = 'Please take a minute to rate it:';
exports.yes = 'Yes, of course';
exports.later = 'Ask me later';
exports.never = 'No, thank you';

exports.appleId = null;
exports.daysBetween = 3;
exports.pointsBetween = 3;
exports.eachVersion = false;

exports.plus = plus;
exports.test = test;
exports.ask = ask;
exports.open = open;
exports.reset = reset;

exports.listener = null;