/***************************************************


Usage:
	// the following call will return null
	Ti.App.Cache.get('my_data');

	// now we'll cache object under "my_data" key for 5 minutes
	// (if you do not specify, the default cache time is 5 minutes)
	var my_javascript_object = { property: 'value' };
	Ti.App.Cache.put('my_data', my_javascript_object);

	// returns cached object
	var cached_obj = Ti.App.Cache.get('my_data');

	// cache another object (a xml document) for 1 hour
	// (you can specify different cache expiration times then 5 minutes)
	Ti.App.Cache.put('another_data', xml_document, 3600);

	// the following call will delete an object from cache
	Ti.App.Cache.del('my_data');
***************************************************/

(function(){
	

	Ti.App.appCache = function() {
		var   get, put, del;   //getTS : get last updated timestamp for that category



		current_timestamp = function() {
			var value = Math.floor(new Date().getTime() / 1000);
			Ti.API.debug("[APP-CACHE] current_timestamp=" + value);
			return value;
		};

		get = function(key) {
			
		
			
			var result = null;
			if (Titanium.App.Properties.hasProperty(key)) {
				Ti.API.info('[APP-CACHE] HIT, key[' + key + ']');
				result = Titanium.App.Properties.getObject(key);
			} 
			return result;
		};
		put = function(key, value) {
			
			Ti.API.info('[APP-CACHE] PUT: time=' + current_timestamp() + ', key= ' + key);
			Titanium.App.Properties.setObject(key,value);
		};

		del = function(key) {
			Ti.API.info('[APP-CACHE] DELETED key[' + key + ']');
			Titanium.App.Properties.removeProperty(key);
		};
		
		
		return function() {
	
			return {
				get: get,
				put: put,
				del: del
			};
		}();
		
	}();
	
})();