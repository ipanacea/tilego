/**
* logger wrapper object for titanium logging.
* example:
 		

* @class Logger
* @module tilego
* @submodule Util
*/
var Logger = function(filename){
	this.filename =  filename;
	this.logit = true;
};

/**
 	* logs at info level
 	*
 	* @method info
 	* @param {String} message to be logged
 	*/
Logger.prototype.info = function(message){
	if (this.logit) {
		var printMessage = prepareLogMessage(message,this.filename);
		Ti.API.info(printMessage);
	}
};

/**
 	* logs at warn level
 	*
 	* @method warn
 	* @param {String} message to be logged
 	*/
Logger.prototype.warn = function(message){
	if (this.logit) {
		var printMessage = prepareLogMessage(message,this.filename);
		Ti.API.warn(printMessage);
	}
};

/**
 	* logs at debug level
 	*
 	* @method debug
 	* @param {String} message to be logged
 	*/
Logger.prototype.debug = function(message){
	if (this.logit) {
		var printMessage = prepareLogMessage(message,this.filename);
		Ti.API.debug(printMessage);
	}
};

/**
 	* logs at error level
 	*
 	* @method error
 	* @param {String} message to be logged
 	*/
Logger.prototype.error = function(message){
	if (this.logit) {
		var printMessage = prepareLogMessage(message,this.filename);
		Ti.API.error(printMessage);
	}
};


//private functions
var prepareLogMessage = function(msg,filename) {
	return "[ " + (new Date()).toISOString() + " ]" + " [ " + filename + " ] : " + msg ;
	
};

module.exports = Logger;
