
var Validate = (function() {
	var messages = {
    	required           : L('validationRequiredField', 'The %s field is required.'),
    	matches            : L('validationMatchField', 'The %s field does not match the %p field.'),
    	valid_email        : L('validationValidEmail', 'The %s field must contain a valid email address.'),
    	valid_emails       : L('validationValidEmails', 'The %s field must contain all valid email addresses.'),
    	min_length         : L('validationMinLength', 'The %s field must be at least %p characters in length.'),
    	max_length         : L('validationMaxLength', 'The %s field must not exceed %p characters in length.'),
    	exact_length       : L('validationExactLength', 'The %s field must be exactly %p characters in length.'),
    	greater_than       : L('validationGreaterThan', 'The %s field must contain a number greater than %p.'),
    	is_checked         : L('validationIsChecked', '%s has to be set.'),
    	agree_on           : L('validationAgreeOn', 'You have to agree to %s.'),
    	age_at_least       : L('validationAgeAtLeast', 'You must be at least %p years old to continue!'),
    	less_than          : L('validationLessThan', 'The %s field must contain a number less than %p.'),
    	alpha              : L('validationAlpha', 'The %s field must only contain alphabetical characters.'),
    	alpha_numeric      : L('validationAlphaNumeric', 'The %s field must only contain alpha-numeric characters.'),
    	alpha_dash         : L('validationDash', 'The %s field must only contain alpha-numeric characters, underscores, and dashes.'),
    	numeric            : L('validationNumeric', 'The %s field must contain only numbers.'),
    	integer            : L('validationInteger', 'The %s field must contain an integer.'),
    	decimal            : L('validationDecimal', 'The %s field must contain a decimal number.'),
    	is_natural         : L('validationIsNatural', 'The %s field must contain only positive numbers.'),
    	is_natural_no_zero : L('validationIsNaturalNoZero', 'The %s field must contain a number greater than zero.'),
    	valid_ip           : L('validationValidIp', 'The %s field must contain a valid IP.'),
    	valid_base64       : L('validationValidBase64', 'The %s field must contain a base64 string.'),
    	valid_credit_card  : L('validationValidCreditCard', 'The %s field must contain a valid credit card number.'),
    	valid_url          : L('validationValidUrl', 'The %s field must contain a valid URL.'),
    	valid_zip_code     : L('validationValidZipCode', 'The %s field must be a valid Zip Code™.')
	};

	var ruleRegex = /^(.+?)\[(.+)\]$/,
        numericRegex = /^[0-9]+$/,
        integerRegex = /^\-?[0-9]+$/,
        decimalRegex = /^\-?[0-9]*\.?[0-9]+$/,
        emailRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,
        alphaRegex = /^[a-z]+$/i,
        alphaNumericRegex = /^[a-z0-9]+$/i,
        alphaDashRegex = /^[a-z0-9_\-]+$/i,
        naturalRegex = /^[0-9]+$/i,
        naturalNoZeroRegex = /^[1-9][0-9]*$/i,
        ipRegex = /^((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){3}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})$/i,
        base64Regex = /[^a-zA-Z0-9\/\+=]/i,
        numericDashRegex = /^[\d\-\s]+$/,
        urlRegex = /^((http|https):\/\/(\w+:{0,1}\w*@)?(\S+)|)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?$/;
        
     return {
     	messages : messages,
     	
     	required: function(value) {
                 return (value !== null && value !== '');
        },

        "default": function(value, defaultName){
            return value !== defaultName;
        },

        valid_email: function(value) {
            return emailRegex.test(value);
        },

        valid_emails: function(value) {
            var result = value.split(",");

            for (var i = 0, resultLength = result.length; i < resultLength; i++) {
                if (!emailRegex.test(result[i])) {
                    return false;
                }
            }

            return true;
        },

        min_length: function(value, length) {
            if (!numericRegex.test(length)) {
                return false;
            }

            return (value.length >= parseInt(length, 10));
        },

        max_length: function(value, length) {
            if (!numericRegex.test(length)) {
                return false;
            }

            return (value.length <= parseInt(length, 10));
        },

        exact_length: function(value, length) {
            if (!numericRegex.test(length)) {
                return false;
            }

            return (value.length === parseInt(length, 10));
        },

        greater_than: function(value, param) {
            if (!decimalRegex.test(value)) {
                return false;
            }

            return (parseFloat(value) > parseFloat(param));
        },

        less_than: function(value, param) {
            if (!decimalRegex.test(value)) {
                return false;
            }

            return (parseFloat(value) < parseFloat(param));
        },

        alpha: function(value) {
            return (alphaRegex.test(value));
        },

        alpha_numeric: function(value) {
            return (alphaNumericRegex.test(value));
        },

        alpha_dash: function(value) {
            return (alphaDashRegex.test(value));
        },

        numeric: function(value) {
            return (numericRegex.test(value));
        },

        integer: function(value) {
            return (integerRegex.test(value));
        },

        decimal: function(value) {
            return (decimalRegex.test(value));
        },

        is_natural: function(value) {
            return (naturalRegex.test(value));
        },

        is_natural_no_zero: function(value) {
            return (naturalNoZeroRegex.test(value));
        },

        valid_ip: function(value) {
            return (ipRegex.test(value));
        },

        valid_base64: function(value) {
            return (base64Regex.test(value));
        },

        valid_url: function(value) {
            return (urlRegex.test(value));
        },

        valid_credit_card: function(value){
            // Luhn Check Code from https://gist.github.com/4075533
            // accept only digits, dashes or spaces
            if (!numericDashRegex.test(value)) return false;

            // The Luhn Algorithm. It's so pretty.
            var nCheck = 0, nDigit = 0, bEven = false;
            var strippedField = value.replace(/\D/g, "");

            for (var n = strippedField.length - 1; n >= 0; n--) {
                var cDigit = strippedField.charAt(n);
                nDigit = parseInt(cDigit, 10);
                if (bEven) {
                    if ((nDigit *= 2) > 9) nDigit -= 9;
                }

                nCheck += nDigit;
                bEven = !bEven;
            }

            return (nCheck % 10) === 0;
        },
     };
}());
    
    
module.exports = Validate;
    