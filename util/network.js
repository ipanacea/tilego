/***************************************************
# XHR:
XHR is a wrapper around Titanium's HTTPClient. It works perfectly with REST API endpoints and has a built in cache system(file system based) that you can use for your requests. But it also can be use for any HTTP requests, you can even cache remote images.

## Usage:
In your app.js (or elsewhere), call:

```javascript
var XHR = require("/xhr");
var xhr = new XHR();
xhr.get("http://freegeoip.net/json/", onSuccessCallback, onErrorCallback, options);
```

A quick explanation of the arguments in the `get()` mehtod of the previous code would look like this:

* **url**: (required) The URL where the API endpoint or content is located 
* **successCallback**: (required) Function to execute if the request succeeds 
* **errorCallback**: (optional) Function to execute if the request fails 
* **options**: (optional) Huge set of options for your request, please see the [
## Helpers
Apart from the RESTful way of interacting with your API endpoints, this module also includes the following helper methods:

### clear(url)

* **url**: (required) The URL you want removed from the cache manager

Finds the cached document of the given url (if any) and removes it from the cache manager. This method is useful if you are not satisfied with the results you got at the time.

### clean()
Goes through all the cached documents and delete everything that has been expired (if their TTL timestamp is less than the current time)

This method returns the count of deleted documents

### purge()
Goes through all the documents and deletes everything

This method returns the count of deleted documents

####################### EXAMPLE ###############################
var XHR = require("/xhr");
var xhr = new XHR();

// Normal plain old request without caching
xhr.get("http://freegeoip.net/json/", onSuccessCallback, onErrorCallback);

// Normal plain old request with a 5mins caching
xhr.get("http://freegeoip.net/json/", onSuccessCallback, onErrorCallback, { ttl: 5 });

// Request a remote image with 60 mins caching
// note that I am passing the contentType as an image, this tells the library to
// respond with binary content instead of plain text
xhr.get("http://upload.wikimedia.org/wikipedia/commons/thumb/f/fa/Apple_logo_black.svg/500px-Apple_logo_black.svg.png", onImageSuccess, onErrorCallback, { ttl: 60, contentType: "image/png" });

function onSuccessCallback(e) {
	// Handle your request in here
	// the module will return an object with two properties
	// data (the actual data retuned
	// status ('ok' for normal requests and 'cache' for requests cached
	Titanium.API.info(e);
};

function onImageSuccess(e) {
	// Assign the image blob located in e.data directly to an image view
};

function onErrorCallback(e) {
	// Handle your errors in here
};

// Delete cached image file
xhr.clear("http://upload.wikimedia.org/wikipedia/commons/thumb/f/fa/Apple_logo_black.svg/500px-Apple_logo_black.svg.png");

// Delete all expired documents (this method should be called at least once in your app)
xhr.clean();

// Delete all cached documents (expired or not, be very careful using this method)
xhr.purge();

####################### END of EXAMPLE ###############################





***************************************************/
// Create the cache manager (a shared object)
var cacheManager = Titanium.App.Properties.getObject("cachedXHRDocuments", {});
var serverUrl ={};
serverUrl.androidEmulator = "http://10.0.3.2:8000";
serverUrl.iosSimulator  = "http://127.0.0.1:8000";//"http://dev-myfooty.herokuapp.com";
serverUrl.djangoServer = "http://prod-myfooty.herokuapp.com";
var connectionParams = {};
connectionParams.network = false;
connectionParams.connectedServer = false;


// Public functions
// ================

// GET 
// @url (string) URL to fetch
// @onSuccess (function) success callback
// @onError (function) error callback
// @extraParams (object) 
exports.get = function(url, onSuccess, onError, extraParams) {
	// Debug
	url = encodeURI(url);
	
	
	// Create some default params
	var onSuccess = onSuccess || function(){};
	var onError = onError || function(){};
	var extraParams = extraParams || {};
	extraParams.async = (extraParams.hasOwnProperty('async')) ? extraParams.async : true;
	extraParams.ttl = extraParams.ttl || false; 
	extraParams.shouldAuthenticate = extraParams.shouldAuthenticate || false; // if you set this to true, pass "username" and "password" as well
	extraParams.contentType = extraParams.contentType || "application/json";
	var cache = readCache(url);
	// If there is nothing cached, send the request
	if (!extraParams.ttl || cache == 0) {
		
		// Create the HTTP connection
		var xhr = Titanium.Network.createHTTPClient({
			enableKeepAlive: false
		});
		
		// Create the result object
		var result = {};
		connectionParams.network = Titanium.Network.online;
		result.network = connectionParams.network;
		
		if(connectionParams.network) {
			Ti.API.debug("Connectivity is Up!");
		}
		else {
			Ti.API.debug("Connectivity is down!");
		}
		
		// Open the HTTP connection
		xhr.open("GET", url, extraParams.async);
		xhr.setRequestHeader('Content-Type', extraParams.contentType);

		// If we need to authenticate
		if (extraParams.shouldAuthenticate) {
			var authstr = 'Basic ' + Titanium.Utils.base64encode(extraParams.username + ':' + extraParams.password); 
			xhr.setRequestHeader('Authorization', authstr);
		}
	
		// When the connection was successful
		xhr.onload = function() {
			connectionParams.connectedServer = true;
			result.connectedServer = connectionParams.connectedServer;
			
			// Check the status of this
			result.status = xhr.status == 200 ? "ok" : xhr.status;
			
			//connectionStarted = true;
			
			// Check the type of content we should serve back to the user
			if (extraParams.contentType.indexOf("application/json") != -1) {
				result.data = xhr.responseText;
			} else if (extraParams.contentType.indexOf("text/xml") != -1) {
				result.data = xhr.responseXML;
			} else {
				result.data = xhr.responseData;
			}
						
			onSuccess(result);
		
			// Cache this response
			if(xhr.status == 200 && extraParams.ttl)
			writeCache(result.data, url, extraParams.ttl);
		};
	
		// When there was an error
		xhr.onerror = function(e) {
			Ti.API.info("in onerror status = " + xhr.status);
			result.connectedServer = connectionParams.connectedServer;
			
			if(!Titanium.Network.online) {
				result.statusText = "Check your network connection!";
			}
			
			else if(Titanium.Network.online && connectionParams.connectedServer) {
				result.statusText = "Oops. Something went wrong!\nPlease retry!";
			}
			
			else if(xhr.status == 404) {
				result.statusText = "The dreaded 404 Page Not Found!";
			}
			else if(xhr.status == 0 && !connectionParams.connectedServer){
				result.statusText = "Can't connect to server. Is your network up?";
			}
			else if(xhr.status == 500){
				result.statusText = "Congratulations! You've crashed our server :(";
			}
			else if(xhr.status == 503){
				result.statusText = "Oops. The server couldn't handle your request\nPlease retry!";
			}
			else{
				result.statusText = "Oops. Something went wrong!\nPlease retry!";
			}

			// Check the status of this
			result.status = "error";
			result.data = e;
			result.code = xhr.status;
			onError(result);
		};

		xhr.onreadystatechange = function () {
    		if (xhr.readyState == 4) {
			//indicator.stop();
	
        		
        		if (xhr.status == 200) {
           		 // Should go into onload
        		} else if (xhr.status == 500) {
            		// Can't even connect
            		//onError(result);
            		Ti.API.info("Error status 500 = " + xhr.status);
        		}
    		}
		};
		Titanium.API.info("Send XHR: "+url);
		xhr.send();
	} else {
		var result = {};
		Ti.API.info("Got data from cache for: "+ url);
		result.status = "cache";
		result.data = cache;
		
		onSuccess(result);
	}	
};

// POST requests
// @url (string) URL to fetch
// @data (object)
// @onSuccess (function) success callback
// @onError (function) error callback
// @extraParams (object)
exports.post = function(url, data, onSuccess, onError, extraParams) {
	
	// Debug
	Titanium.API.info(url + " " + JSON.stringify(data));
	
	// Create some default params
	var onSuccess = onSuccess || function(){};
	var onError = onError || function(){};
	var extraParams = extraParams || {};
	extraParams.async = (extraParams.hasOwnProperty('async')) ? extraParams.async : true;
	extraParams.shouldAuthenticate = extraParams.shouldAuthenticate || false; // if you set this to true, pass "username" and "password" as well
	extraParams.contentType = extraParams.contentType || "application/json";
	
	// Create the HTTP connection
	var xhr = Titanium.Network.createHTTPClient({
		enableKeepAlive: false
	});
	// Create the result object
	var result = {};
	
	// Open the HTTP connection
	xhr.open("POST", url, extraParams.async);
	//xhr.setRequestHeader('Content-Type', extraParams.contentType);
	
	// If we need to authenticate
	if (extraParams.shouldAuthenticate) {
		var authstr = 'Basic ' + Titanium.Utils.base64encode(extraParams.username + ':' + extraParams.password); 
		xhr.setRequestHeader('Authorization', authstr);
	}
	
	// When the connection was successful
	xhr.onload = function() {
		// Check the status of this
		result.status = xhr.status == 200 ? "ok" : xhr.status;
		result.data = xhr.responseText;
		
		onSuccess(result);
	};
	
	// When there was an error
	xhr.onerror = function(e) {
		// Check the status of this		
		result.status = "error";
		result.data = e.error;
		result.code = xhr.status;
		onError(result);
	};
	
	xhr.send(data);
};

// PUT requests
// @url (string) URL to fetch
// @data (object)
// @onSuccess (function) success callback
// @onError (function) error callback
// @extraParams (object)
exports.put = function(url, data, onSuccess, onError, extraParams) {
	// Create some default params
	var onSuccess = onSuccess || function(){};
	var onError = onError || function(){};
	var extraParams = extraParams || {};
	extraParams.async = (extraParams.hasOwnProperty('async')) ? extraParams.async : true;
	extraParams.shouldAuthenticate = extraParams.shouldAuthenticate || false; // if you set this to true, pass "username" and "password" as well
	extraParams.contentType = extraParams.contentType || "application/json";
	
	// Create the HTTP connection
	var xhr = Titanium.Network.createHTTPClient({
		enableKeepAlive: false
	});
	// Create the result object
	var result = {};
	
	// Open the HTTP connection
	xhr.open("PUT", url, extraParams.async);
	xhr.setRequestHeader('Content-Type', extraParams.contentType);
	
	// If we need to authenticate
	if (extraParams.shouldAuthenticate) {
		var authstr = 'Basic ' + Titanium.Utils.base64encode(extraParams.username + ':' + extraParams.password); 
		xhr.setRequestHeader('Authorization', authstr);
	}
	
	// When the connection was successful
	xhr.onload = function() {
		// Check the status of this
		result.status = xhr.status == 200 ? "ok" : xhr.status;
		result.data = xhr.responseText;
		
		onSuccess(result);
	};
	
	// When there was an error
	xhr.onerror = function(e) {
		// Check the status of this
		result.status = "error";
		result.data = e.error;
		result.code = xhr.status;
		onError(result);
	};
	
	xhr.send(data);
};

// DELETE requests
// @url (string) URL to fetch
// @onSuccess (function) success callback
// @onError (function) error callback
// @extraParams (object)
exports.destroy = function(url, onSuccess, onError, extraParams) {
	// Debug
	Titanium.API.info(url);
	
	// Create some default params
	var onSuccess = onSuccess || function(){};
	var onError = onError || function(){};
	var extraParams = extraParams || {};
	extraParams.async = (extraParams.hasOwnProperty('async')) ? extraParams.async : true;
	extraParams.shouldAuthenticate = extraParams.shouldAuthenticate || false; // if you set this to true, pass "username" and "password" as well
	extraParams.contentType = extraParams.contentType || "application/json";
	
	// Create the HTTP connection
	var xhr = Titanium.Network.createHTTPClient({
		enableKeepAlive: false
	});
	// Create the result object
	var result = {};
	
	// Open the HTTP connection
	xhr.open("DELETE", url, extraParams.async);
	xhr.setRequestHeader('Content-Type', extraParams.contentType);
	
	// If we need to authenticate
	if (extraParams.shouldAuthenticate) {
		var authstr = 'Basic ' + Titanium.Utils.base64encode(extraParams.username + ':' + extraParams.password); 
		xhr.setRequestHeader('Authorization', authstr);
	}
	
	// When the connection was successful
	xhr.onload = function() {
		// Check the status of this
		result.status = xhr.status == 200 ? "ok" : xhr.status;
		result.data = xhr.responseText;
		
		onSuccess(result);
	};
	
	// When there was an error
	xhr.onerror = function(e) {
		// Check the status of this
		result.status = "error";
		result.data = e.error;
		result.code = xhr.status;
		onError(result);
	};
	
	xhr.send();
};

// Helper functions
// =================

// Removes the cached content of a given URL (this is useful if you are not satisfied with the data returned that time)
exports.clear = function(url) {
		
	if (url) {
		// Hash the URL
		var hashedURL = Titanium.Utils.md5HexDigest(url);
		// Check if the file exists in the manager
		var cache = cacheManager[hashedURL];
		
		// If the file was found
		if (cache) {
			// Delete references and file
			var file = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationCacheDirectory, hashedURL);
			// Delete the record and file
			delete cacheManager[hashedURL];
			file.deleteFile();	
			
			// Update the cache manager
			updateCacheManager();
			
			//Titanium.API.info("REMOVED CACHE FILE " + hashedURL);
		}
	} 
	
};

// Removes all the expired documents from the manager and the file system
exports.clean = function() {
	
	var nowInMilliseconds = new Date().getTime();
	var expiredDocuments = 0;
	
	for (var key in cacheManager) {
		var cache = cacheManager[key];
	   
		if(cache.timestamp <= nowInMilliseconds){
			// Delete references and file
			var file = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationCacheDirectory, key);
			// Delete the record and file
			delete cacheManager[key];
			file.deleteFile();	
			
			// Update the cache manager
			updateCacheManager();
			
			// Update the deleted documents count
			expiredDocuments = expiredDocuments + 1;
			
			//Titanium.API.info("REMOVED CACHE FILE " + cachedDocuments[i].file);
		}

	}
	
	// Return the number of files deleted
	return expiredDocuments;	
};

// Removes all documents from the manager and the file system
exports.purge = function() {
	
	var purgedDocuments = 0;
	
	for (var key in cacheManager) {
		var cache = cacheManager[key];
		// Delete references and file
		var file = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationCacheDirectory, key);
		// Delete the record and file
		delete cacheManager[key];
		file.deleteFile();	

		// Update the cache manager
		updateCacheManager();

		// Update the deleted documents count
		purgedDocuments = purgedDocuments + 1;

		//Titanium.API.info("REMOVED CACHE FILE " + cachedDocuments[i].file);

	}
	
	// Return the number of files deleted
	return purgedDocuments;	
};


// app sepcific functions 
exports.getServerUrl = function(){
	if( $tl.device.properties.platformModel === "Simulator"){
		return serverUrl.iosSimulator;
	}
	else if($tl.device.properties.platformModel.indexOf("Google Nexus") > -1){
		return serverUrl.androidEmulator;
	}
	else
		return serverUrl.djangoServer;
};

exports.getLeagueData=function(leagueName,onSuccess, onError, extraParams){
	var uriEncodedLeague = encodeURI(leagueName);
	var url = this.getServerUrl() + "api/v1/match/?format=json&competitionName__iexact="+uriEncodedLeague;
	this.get(url,onSuccess, onError, extraParams);
};




// Private functions
// =================

readCache = function(url) {
	// Hash the URL
	var hashedURL = Titanium.Utils.md5HexDigest(url);
	
	// Check if the file exists in the manager
	var cache = cacheManager[hashedURL];
	// Default the return value to false
	var result = false;
	
	//Titanium.API.info("CHECKING CACHE");
	
	// If the file was found
	if (cache) {
		// Fetch a reference to the cache file
		var file = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationCacheDirectory, hashedURL);
				
		// Check that the TTL is further than the current date
		if (cache.timestamp >= new Date().getTime()) {
			//Titanium.API.info("CACHE FOUND");

			// Return the content of the file
			result = file.read();

		} else {
			//Titanium.API.info("OLD CACHE");

			// Delete the record and file
			delete cacheManager[hashedURL];
			file.deleteFile();	
			
			// Update the cache manager
			updateCacheManager();	
		}
	} else {
		//Titanium.API.info("CACHE " + hashedURL + " NOT FOUND");
	}
		
	return result;
};

updateCacheManager = function(){
	Titanium.App.Properties.setObject("cachedXHRDocuments", cacheManager);
};

writeCache = function(data, url, ttl) {
		
	Titanium.API.debug("WRITING CACHE");

	// hash the url
	var hashedURL = Titanium.Utils.md5HexDigest(url);
		
	// Write the file to the disk
	var file = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationCacheDirectory, hashedURL);
	
	// Write the file to the disk
	// TODO: There appears to be a bug in Titanium and makes the method
	// below always return false when dealing with binary files
	file.write(data);
	
	// Insert the cached object in the cache manager
	cacheManager[hashedURL] = { "timestamp": (new Date().getTime()) + (ttl*60*1000) };
	updateCacheManager();
		
	//Titanium.API.info("WROTE CACHE");	
};

